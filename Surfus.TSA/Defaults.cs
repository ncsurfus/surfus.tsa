﻿using System;

namespace Surfus.TSA
{
    public static class Defaults
    {
        public static readonly string[] prompts = { "#", ">", ":", "$" };
        public const string EOL = "\r\n";
        public const int readSleepTime = 25;
    }
}
