﻿using System;

namespace Surfus.TSA
{
    public static class Alerts
    {
        public const string badUsername = "Bad Username";
        public const string badPassword = "Bad Password";
        public const string badHostname = "Bad Hostname";
        public const string connectionFailed = "Connection Failed";
        public const string connectionSuccessful = "Connection Successful";
        public const string notConnected = "Failure: Not Connected";
        public const string disconnectedSuccessful = "Disconnection Successful";
        public const string alreadyConnected = "Failure: Already Connected";
        public const string alreadyDisconnected = "Failure: Already Disconnected";
        public const string expectTimedOut = "Expect Timed Out";
    }
}
