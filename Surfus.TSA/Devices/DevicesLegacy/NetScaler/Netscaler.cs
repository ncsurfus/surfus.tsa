using System;
using System.Text;
using System.Security;
using System.Collections.Generic;
using Surfus.TSA.Extensions;

namespace Surfus.TSA.Devices
{
	public partial class Netscaler : DeviceConnection
	{
		public static string MoreReturn = null;
		public static string MoreSequence = null;

		public override Terminal Connection
		{
			get
			{
				return _connection;
			}

			set
			{
				if(value != null)
				{
					_connection = value;
					//Add Prompts and more-sequence.
					if(!_connection.Prompts.Contains(">"))
					{
						_connection.Prompts.Add(">");
					}

					_connection.MoreReturn = MoreReturn;
					_connection.MoreSequence = MoreSequence;
				}
			}
		}

		public Netscaler(Terminal DeviceConnection)
		{
			Connection = DeviceConnection;
			Name = "NetScaler Generic";
		}

		public Netscaler(bool SSH, string Host, string Username, string Password)
		{
			if(SSH)
			{
				try
				{
					Connection = new Terminal(new sshTerminal(Host));
					Connection.Open(Username, Password);
				}
				catch
				{
					return;
				}
			}
			else
			{
				try
				{
					Connection = new Terminal(new TelnetTerminal(Host));
					Connection.Open(Username, Password);
				}
				catch
				{
					return;
				}
			}

			Name = "NetScaler Generic";
		}
	}
}