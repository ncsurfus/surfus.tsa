using System;
using System.Text;
namespace Surfus.TSA.Devices
{
	public partial class Netscaler : DeviceConnection
	{
		public class Service
		{
			public string Name { get; private set; }
			public string IPAddress { get; private set; }
			public string Port { get; private set; }
			public string State { get; private set; }
			public string ServerName { get; private set; }

			public Service(string name, string ipaddress, string port, string state, string servername)
			{
				Name = name;
				IPAddress = ipaddress;
				Port = port;
				State = state;
				ServerName = servername;
			}
		}
		
		public class VServer
		{
			public string Name { get; private set; }
			public string IPAddress { get; private set; }
			public string Port { get; private set; }
			public string State { get; private set; }
			public string Type { get; private set; }

			public VServer(string name, string ipaddress, string port, string state, string type)
			{
				Name = name;
				IPAddress = ipaddress;
				Port = port;
				State = state;
				Type = type;
			}
		}
	}
}