using System;
using System.Text;
using System.Collections.Generic;
namespace Surfus.TSA.Devices
{
	public partial class Netscaler : DeviceConnection
	{
		public VServer GetVServer(string Name_Or_Address)
		{
			if(Connection != null && Connection.IsConnected && !String.IsNullOrEmpty(Name_Or_Address))
			{
				string[] Lines = Connection.SendCommand("show vserver").Split('\n');
				bool EnteredVServer = false;
				int Begin = 0;

				//Skip inital command
				for(int i = 1; i < Lines.Length; i++)
				{
					if(!EnteredVServer)
					{
						if(!Lines[i].StartsWith("	") && Lines[i].Contains(Name_Or_Address))
						{
							//Ensure we have the correct line. 172.16.12.20 vs 172.16.12.200
							int NextCharIndex = Lines[i].IndexOf(Name_Or_Address) + Name_Or_Address.Length;
							if(NextCharIndex < Lines[i].Length && Lines[i][NextCharIndex] == ' ' || Lines[i][NextCharIndex] == ':')
							{
								//We've found the Name_Or_Address we are looking for.
								Begin = i;
								EnteredVServer = true;
							}
						}
					}
					else
					{
						//Hit the end of our service.
						if(!Lines[i].StartsWith("	"))
							return VServerBuilder(Lines, Begin, i--);
					}
				}
			}

			return null;
		}

		private VServer VServerBuilder(string[] Lines)
		{
			return VServerBuilder(Lines, 0, Lines.Length);
		}

		private VServer VServerBuilder(string[] Lines, int Begin, int End)
		{
			//Values to fill
			string ServiceName = null;
			string IPAddress = null;
			string Port = null;
			string State = null;
			string Type = null;
			//string Default = "N/A";

			//Skip inital command
			for(int i = Begin; i < End; i++)
			{
				if(!Lines[i].StartsWith("	"))//Header
				{
					//Get VServer Name
					int BeginServiceName = Lines[i].IndexOf("	") + 1;//Skip Tab
					int ServiceNameLength = Lines[i].IndexOf(" (") - BeginServiceName;
					if(BeginServiceName > 0 && ServiceNameLength > 0 && BeginServiceName + ServiceNameLength < Lines[i].Length)
					{
						ServiceName = Lines[i].Substring(BeginServiceName, ServiceNameLength);
						//Find IP Address
						int BeginIP = BeginServiceName + ServiceNameLength + 2;//Skip ' ('
						int IPLength = Lines[i].IndexOf(':', BeginIP) - BeginIP;
						if(BeginIP + IPLength < Lines[i].Length)
						{
							IPAddress = Lines[i].Substring(BeginIP, IPLength);
							//Find Port
							int BeginPort = BeginIP + IPLength + 1;//Skip :
							int PortLength = Lines[i].IndexOf(')', BeginPort) - BeginPort;
							if(BeginPort + PortLength < Lines[i].Length)
							{
								Port = Lines[i].Substring(BeginPort, PortLength);
								//Find Type
								int BeginType = Lines[i].IndexOf("	Type: ", BeginPort + PortLength) + 7;//Skip '	Type: '
								int TypeLength = Lines[i].Length - BeginType;
								Type = Lines[i].Substring(BeginType, TypeLength).TrimEnd();
							}
						}
					}
				}
				else
				{
					if(Lines[i].StartsWith("	State: "))
						State = Lines[i].Substring("	State: ".Length);
				}
			}

			if(ServiceName != null && IPAddress != null && Port != null && State != null && Type != null)
			{
				return new VServer(ServiceName, IPAddress, Port, State, Type);
			}
			else
			{
				return null;
			}
		}
	}
}