using System;

namespace Surfus.TSA.Devices
{
	public abstract class NetworkDevice : DeviceConnection
	{
        public abstract string GetPortFromMacAddressTable(csaMAC macaddress);
        public abstract csaMAC GetMacAddressFromARPTable(csaIP ipaddress);
        public abstract csaIP[] GetIPAddressesFromARPTable(csaMAC macaddress);
        public abstract string GetPortFromARPTable(AddressBase address);
        public abstract string GetPortConfiguration(string Port);
        public abstract string[] GetActiveEtherChannelPorts(string EtherChannel);
        public abstract csaIP GetPortPrimaryIP(string Port);
        public abstract csaIP[] GetPortIPAddresses(string Port);
        public abstract ARPEntry GetARPEntry(Surfus.TSA.csaIP ipaddress);
        public abstract ARPEntry[] GetARPEntries(Surfus.TSA.csaMAC macaddress);
        public abstract MACEntry GetMACEntry(csaMAC address);
        public abstract CDPEntry GetCDPEntry(string Port);
	}
}