using System;
using System.Collections.Generic;

namespace Surfus.TSA
{
	public class ARPEntry
	{
		public csaIP Ipaddress {get; set;}
		public csaMAC Macaddress {get; set;}
		public string Port {get; set;}

		public ARPEntry(string ipaddress, string macaddress, string port) : this(new csaIP(ipaddress), new csaMAC(macaddress), port){}
		
		public ARPEntry(csaIP ipaddress, csaMAC macaddress, string port)
		{
			Ipaddress = ipaddress;
			Macaddress = macaddress;
			Port = port;
		}
	}
	
	public class MACEntry
	{
		public string Vlan {get; set;}
		public csaMAC Macaddress {get; set;}
		public string Port {get; set;}

		public MACEntry(string vlan, string macaddress, string port) : this(vlan, new csaMAC(macaddress), port){}
		
		public MACEntry(string vlan, csaMAC macaddress, string port)
		{
			Vlan = vlan;
			Macaddress = macaddress;
			Port = port;
		}
	}
	
	public class CDPEntry
	{
		public string RemotePort {get; set;}
		public string LocalPort {get; set;}
        public string[] IPAddresses {get; set;}
		public string Platform {get; set;}
        public string DeviceID { get; set; }

        public CDPEntry(string deviceid, string remoteport, string localport, string platform, string[] ipaddress)
		{
            DeviceID = deviceid;
			RemotePort = remoteport;
			LocalPort = localport;
            IPAddresses = ipaddress;
			Platform = platform;
		}
	}
}