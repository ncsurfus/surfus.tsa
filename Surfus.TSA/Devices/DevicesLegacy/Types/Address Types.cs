using System;
using System.Net.NetworkInformation;
using System.Globalization;
using Surfus.TSA.Extensions.Physical;

namespace Surfus.TSA
{
    public static class MacAddress
    {
        public static bool TryParse(out PhysicalAddress Physical, string MacAddress)
        {
            try
            {
                Physical = PhysicalAddress.Parse(MacAddress.TransformParse());
                return true;
            }
            catch
            {
                Physical = null;
                return false;
            }
        }
    }

	public class AddressBase
	{
		public string Address { get; set; }
		public override string ToString()
		{
			return Address;
		}

		public AddressBase(string address)
		{
			Address = address;
		}
	}

	public class csaIP : AddressBase
	{
		public csaIP(string address) : base (address)
		{
            
		}
	}

	public class csaMAC : AddressBase
	{
        public string TransformHHH
        {
            get
            {
                return TransformMacHHH(Address);
            }
        }
        public string TransformHDH
        {
            get
            {
                return TransformMacHDH(Address);
            }
        }
        public bool IsMulticast
        {
            get
            {
                return IsEthernetMulticast(Address);
            }
        }
   
		public csaMAC(string address) : base (address)
		{
		}
        public bool Compare(csaMAC MacAddress)
        {
            return Compare(MacAddress.ToString());
        }
        public bool Compare(string MacAddress)
        {
            return CompareMac(Address, MacAddress);
        }

        public static string TransformMacHDH(string MacAddress)
        {
            if (!String.IsNullOrEmpty(MacAddress))
            {
                char[] FixedMac = new char[13];
                int FixedMacIndex = 0;
                for (int i = 0; i < MacAddress.Length && FixedMacIndex < FixedMac.Length; )
                {
                    if (FixedMacIndex == 6)
                    {
                        FixedMac[FixedMacIndex] = '-';
                        FixedMacIndex++;
                    }
                    else if (char.IsLetterOrDigit(MacAddress, i))
                    {
                        FixedMac[FixedMacIndex] = MacAddress[i];
                        FixedMacIndex++;
                        i++;
                    }
                    else
                    {
                        i++;
                    }
                }

                return new string(FixedMac);
            }

            return null;
        }
        public static string TransformMacHHH(string MacAddress)
        {
            if (!String.IsNullOrEmpty(MacAddress))
            {
                char[] FixedMac = new char[14];
                int FixedMacIndex = 0;
                for (int i = 0; i < MacAddress.Length && FixedMacIndex < FixedMac.Length; )
                {
                    if (FixedMacIndex == 4 || FixedMacIndex == 9)
                    {
                        FixedMac[FixedMacIndex] = '.';
                        FixedMacIndex++;
                    }
                    else if (char.IsLetterOrDigit(MacAddress, i))
                    {
                        FixedMac[FixedMacIndex] = MacAddress[i];
                        FixedMacIndex++;
                        i++;
                    }
                    else
                    {
                        i++;
                    }
                }

                return new string(FixedMac);
            }

            return null;
        }
        public static bool IsEthernetMulticast(string MacAddress)
        {
            if (!String.IsNullOrEmpty(MacAddress) && MacAddress.Length > 1)
            {
                string FirstOctet = MacAddress.Substring(0, 2);
                byte ParsedOctet;
                if (Byte.TryParse(FirstOctet, NumberStyles.HexNumber, null as IFormatProvider, out ParsedOctet))
                {
                    return (ParsedOctet & 1) == 1;
                }
            }

            return false;
        }
        public static bool CompareMac(string MacAddressA, string MacAddressB)
        {
            if (!String.IsNullOrEmpty(MacAddressA) && !String.IsNullOrEmpty(MacAddressB))
            {
                MacAddressA = TransformMacHHH(MacAddressA);
                MacAddressB = TransformMacHHH(MacAddressB);
                if (MacAddressA.ToLower() == MacAddressB.ToLower())
                {
                    return true;
                }
            }
            return false;
        }
	}
	
}