using System;
using System.Text;
using System.Collections.Generic;
using Surfus.Extensions;
using Surfus.TSA.Extensions;

namespace Surfus.TSA.Devices
{
	public class CiscoGeneric : NetworkDevice
	{
		public static string MoreReturn = " ";
		public static string MoreSequence = "--More--";

		public override Terminal Connection
		{
			get
			{
				return _connection;
			}

			set
			{
				if(value != null)
				{
					_connection = value;
					//Add Prompts and more-sequence.
					if(!_connection.Prompts.Contains("#"))
					{
						_connection.Prompts.Add("#");
					}

					_connection.MoreReturn = MoreReturn;
					_connection.MoreSequence = MoreSequence;
				}
			}
		}

		public CiscoGeneric(Terminal DeviceConnection)
		{
			Connection = DeviceConnection;
			Name = "Cisco Generic";
		}

		public CiscoGeneric(bool SSH, string Host, string Username, string Password)
		{
            if (SSH)
            {
                Connection = new Terminal(new sshTerminal(Host));
                Connection.Open(Username, Password);
            }
            else
            {
                Connection = new Terminal(new TelnetTerminal(Host));
                Connection.Open(Username, Password);
            }

			Name = "Cisco Generic";
		}

		public override string GetPortConfiguration(string port)
		{
			if(Connection != null && Connection.IsConnected && !String.IsNullOrEmpty(port))
			{
				return Connection.SendCommand("show run int " + port);
			}  
			return null;
		}

        public override ARPEntry GetARPEntry(Surfus.TSA.csaIP ipaddress)
		{
			string IPAddress = ipaddress.ToString();
			if(Connection != null && Connection.IsConnected && !String.IsNullOrEmpty(IPAddress))
			{
				string[] Lines = Connection.SendCommand("show arp | include Protocol|" + IPAddress + " ").Split('\n');
				ARPEntry[] Entries = GetARPEntries(Lines);
				if(Entries != null)
				{
					return Entries[0];
				}
			}
			return null;
		}

        public override ARPEntry[] GetARPEntries(Surfus.TSA.csaMAC macaddress)
		{
			//Ensure Mac Address fits H.H.H
			string MacAddress = macaddress.TransformHHH;
			if(Connection != null && Connection.IsConnected && !String.IsNullOrEmpty(MacAddress))
			{
				string[] Lines = Connection.SendCommand("show arp | include Protocol| " + MacAddress).Split('\n');
				ARPEntry[] Entries = GetARPEntries(Lines);
				if(Entries != null)
				{
					return Entries;
				}
			}
			return null;
		}
		
		//The Cisco ARP command "show arp" return the following example text:
		//  Protocol  Address          Age (min)  Hardware Addr   Type   Interface
		//  Internet  10.61.133.130           -   0014.f2a1.3340  ARPA   Vlan1
		//So IndexOf("Address") will return the start of the IP Address.
		//Use the command "show arp | inc Protocol|10.61.133.130 " to get both the header and entry.
        public static ARPEntry[] GetARPEntries(string[] Entry)
		{
			List<ARPEntry> ARPEntries = new List<ARPEntry>();
			if(Entry != null && Entry.Length > 0)
			{
				bool FoundHeader = false;
				int AddressIndex, MacIndex, PortIndex, TypeIndex;
				AddressIndex = MacIndex = PortIndex = TypeIndex = 0;
				for(int i = 1; i < Entry.Length; i++)
				{
					if(Entry[i].StartsWith("Protocol") && FoundHeader == false)
					{
						FoundHeader = true;
						AddressIndex = Entry[i].IndexOf("Address");
						MacIndex = Entry[i].IndexOf("Hardware Addr");
						TypeIndex = Entry[i].IndexOf("Type");
						PortIndex = Entry[i].IndexOf("Interface");
					}
					else if(!Entry[i].StartsWith("Protocol") && FoundHeader)
					{
						if(Entry[i].Length > PortIndex)
						{
							int AddressLength = Entry[i].IndexOf(' ', AddressIndex) - AddressIndex;
							int MacLength = Entry[i].IndexOf(' ', MacIndex) - MacIndex;
							int TypeLength = Entry[i].IndexOf(' ', TypeIndex) - TypeIndex;
							int PortLength = Entry[i].Length - PortIndex;
							if(AddressLength > 0 && MacLength > 0 && TypeLength > 0 && PortLength > 0)
							{
								string Address = Entry[i].Substring(AddressIndex, AddressLength);
								string Mac = Entry[i].Substring(MacIndex, MacLength);
								string Port = Entry[i].Substring(PortIndex, PortLength);
								string Type = Entry[i].Substring(TypeIndex, TypeLength);
								ARPEntries.Add(new ARPEntry(new csaIP(Address.Trim()), new csaMAC(Mac.Trim()), Port.Trim()));
							}
						}
					}
				}
			}
			if(ARPEntries.Count > 0)
			{
				return ARPEntries.ToArray();
			}
			else
			{
				return null;
			}
		}

        //MAC Entries are not aligned to the column names like ARP entries are.
        //Furthermore they are different depending on cisco model type.
        //We locate the header name and use that as an index when we split the string based on ' '.
        //Issues arise when column names contain a space. We replace this space with a '-'.
        public static MACEntry[] GetMACEntries(string[] Entry)
        {
            List<MACEntry> MACEntries = new List<MACEntry>();
            if (Entry != null && Entry.Length > 0)
            {
                bool FoundHeader = false;
                int vlanIndex, macIndex, portIndex;
                vlanIndex = macIndex = portIndex = 0;

                for (int i = 0; i < Entry.Length; i++)
                {
                    if (FoundHeader == false)
                    {
                        string Line = Entry[i].ToLower().Replace("mac address", "mac-address");
                        Line = Line.Replace("destination address", "destination-address");
                        Line = Line.Replace("address type", "address-type");
                        Line = Line.Replace("destination port", "destination-port");
                        string[] Arguments = Line.Replace("*", "").Summarize(' ').Trim().Split(' ');
                        for (int j = 0; j < Arguments.Length; j++)
                        {
                            switch (Arguments[j])
                            {
                                case "vlan":
                                    FoundHeader = true;
                                    vlanIndex = j;
                                    break;
                                case "mac-address":
                                    FoundHeader = true;
                                    macIndex = j;
                                    break;
                                case "destination-address":
                                    macIndex = j;
                                    FoundHeader = true;
                                    break;
                                case "port":
                                    portIndex = j;
                                    FoundHeader = true;
                                    break;
                                case "ports":
                                    portIndex = j;
                                    FoundHeader = true;
                                    break;
                                case "destination-port":
                                    portIndex = j;
                                    FoundHeader = true;
                                    break;
                            }
                        }
                    }
                    else
                    {
                        string[] Arguments = Entry[i].Replace("*", "").Summarize(' ').Trim().Split(' ');
                        if (Arguments.Length >= 4)
                        {
                            MACEntries.Add(new MACEntry(Arguments[vlanIndex], Arguments[macIndex], Arguments[portIndex]));
                        }
                    }
                }
            }
            if (MACEntries.Count > 0)
            {
                return MACEntries.ToArray();
            }
            else
            {
                return null;
            }
        }

        public override MACEntry GetMACEntry(csaMAC macaddress)
        {
            //Ensure Mac Address fits H.H.H
            string MacAddress = macaddress.TransformHHH;
            //Only 1 MAC should be associated with a port, except for multicast traffic.
            //Multicast traffic will return null
            if (Connection != null && Connection.IsConnected && !String.IsNullOrEmpty(MacAddress) && !macaddress.IsMulticast)
            {
                string[] Lines = Connection.SendCommand("show mac-address-table | include vlan|Vlan|VLAN|" + MacAddress).Split('\n');
                MACEntry[] Entries = GetMACEntries(Lines);
                if (Entries != null)
                {
                    return Entries[0];
                }
            }
            return null;
        }
		
		public override string GetPortFromMacAddressTable(csaMAC macaddress)
		{
			//Ensure Mac Address fits H.H.H
			string MacAddress = macaddress.TransformHHH;
			//Only 1 MAC should be associated with a port, except for multicast traffic.
			//Multicast traffic will return null
            if (Connection != null && Connection.IsConnected && !String.IsNullOrEmpty(MacAddress) && !macaddress.IsMulticast)
			{
				string[] Lines = Connection.SendCommand("show mac-address-table address " + MacAddress).Split('\n');
				if(Lines != null && Lines.Length > 0)
				{
					for(int i = 0; i < Lines.Length; i++)
					{
						//The port seem to always be listed at the end regardless of cisco device.
                        if (Lines[i].Contains(MacAddress) && !Lines[i].Contains("show mac-address-table address"))
						{
							string[] MacEntry = Lines[i].Trim().Split(' ');
							return MacEntry[MacEntry.Length - 1];
						}
					}
				}
			}

			return null;
		}

		public override string[] GetActiveEtherChannelPorts(string EtherChannel)
		{
			if(Connection != null && Connection.IsConnected && !String.IsNullOrEmpty(EtherChannel))
			{
				List<string> Ports = new List<string>();
				//Strip to EtherChannel Number
                EtherChannel = EtherChannel.getDigits();

				string[] Lines = Connection.SendCommand("show etherchannel sum | inc Po" + EtherChannel).Split('\n');
				if(Lines != null && Lines.Length > 0)
				{
					for(int i = 0; i < Lines.Length; i++)//Ignore first line.
					{
						string[] SplitLine = Lines[i].Split(' ');
						for(int j = 0; j != SplitLine.Length; j++)
						{
							if(SplitLine[j].EndsWith("(P)"))
							{
								Ports.Add(SplitLine[j].Substring(0, SplitLine[j].Length - 3));
							}
							else if(SplitLine[j].EndsWith("(Pd)"))
							{
								Ports.Add(SplitLine[j].Substring(0, SplitLine[j].Length - 4));
							}
						}
					}
				}

				if(Ports.Count > 0)
				{
					return Ports.ToArray();
				}
			}
			return null;
		}

		public override csaIP GetPortPrimaryIP(string Port)
		{
			string[] Lines = GetPortConfiguration(Port).Split('\n');
			if(Lines != null && Lines.Length > 0)
			{
				for(int i = 0; i < Lines.Length; i++)//Ignore first line.
				{
					string Line = Lines[i].Trim();
					if(Line.StartsWith("ip address ") && !Line.EndsWith("secondary"))
					{
						return new csaIP(Line.Split(' ')[2]);
					}
				}
			}

			return null;
		}

		public override csaIP[] GetPortIPAddresses(string Port)
		{
			string[] Lines = GetPortConfiguration(Port).Split('\n');
			List<csaIP> IPAddresses = new List<csaIP>();
			if(Lines != null && Lines.Length > 0)
			{
				for(int i = 0; i < Lines.Length; i++)//Ignore first line.
				{
					string Line = Lines[i].TrimStart();
					if(Line.StartsWith("ip address "))
					{
						IPAddresses.Add(new csaIP(Line.Split(' ')[2]));
					}
				}
			}

			if(IPAddresses.Count > 0)
			{
				return IPAddresses.ToArray();
			}
			return null;
		}

        public override string GetPortFromARPTable(AddressBase address)
        {
            throw new NotImplementedException();
        }

        public override csaIP[] GetIPAddressesFromARPTable(csaMAC macaddress)
        {
            throw new NotImplementedException();
        }

        public override csaMAC GetMacAddressFromARPTable(csaIP ipaddress)
        {
            throw new NotImplementedException();
        }

        public override CDPEntry GetCDPEntry(string Port)
        {
            List<string> IPAddresses = new List<string>();
            CDPEntry NewCDPEntry = new CDPEntry(null, null, null, null, null);
            if (Connection != null && Connection.IsConnected && Port != null)
            {
                string[] Lines = Connection.SendCommand("show cdp neighbor " + Port + " detail").Split('\n');
                for (int i = 0; i < Lines.Length; i++)
                {
                    string Line = Lines[i].Trim();
                    string LineLower = Line.ToLower();
                    if (LineLower.StartsWith("device id: "))
                    {
                        NewCDPEntry.DeviceID = Line.Substring(11);
                    }
                    else if (LineLower.StartsWith("ip address: "))
                    {
                        IPAddresses.Add(Line.Substring(12));
                    }
                    else if (LineLower.StartsWith("interface: "))
                    {
                        int localinterfaceIndex = 11;
                        int localinterfaceendIndex = LineLower.IndexOf(",");
                        int remoteinterfaceIndex = LineLower.LastIndexOf(" ");
                        NewCDPEntry.LocalPort = Line.Substring(localinterfaceIndex, localinterfaceendIndex - localinterfaceIndex);
                        NewCDPEntry.RemotePort = Line.Substring(remoteinterfaceIndex + 1);
                    }
                    else if (LineLower.StartsWith("platform: "))
                    {
                        int platformIndex = 10;
                        int platformendIndex = LineLower.IndexOf(",");
                        NewCDPEntry.Platform = Line.Substring(platformIndex, platformendIndex - platformIndex);
                    }
                }
            }
            NewCDPEntry.IPAddresses = IPAddresses.ToArray();
            return NewCDPEntry;
        }
    }
}