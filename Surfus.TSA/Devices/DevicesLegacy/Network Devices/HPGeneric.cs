using System;
using System.Text;
using System.Collections.Generic;
using Surfus.Extensions;
using Surfus.TSA.Extensions;

namespace Surfus.TSA.Devices
{
	public class HPGeneric : NetworkDevice
	{
		public static string MoreReturn = " ";
		public static string MoreSequence = "-- MORE --";

		public override Terminal Connection
		{
			get
			{
				return _connection;
			}

			set
			{
				if(value != null)
				{
					_connection = value;
					//Add Prompts and more-sequence.
					if(!_connection.Prompts.Contains("#"))
					{
						_connection.Prompts.Add("#");
					}
                    _connection.Prompts.Remove(":");
					_connection.MoreReturn = MoreReturn;
					_connection.MoreSequence = MoreSequence;
				}
			}
		}

		public HPGeneric(Terminal DeviceConnection)
		{
			Connection = DeviceConnection;
			Name = "HP Generic";
		}

		public HPGeneric(bool SSH, string Host, string Username, string Password)
		{
			if(SSH)
			{
				try
				{
                    Connection = new Terminal(new sshTerminal(Host));
					Connection.Open(Username, Password);
				}
				catch
				{
					return;
				}
			}
			else
			{
				try
				{
					Connection = new Terminal(new TelnetTerminal(Host));
					Connection.Open(Username, Password);
				}
				catch
				{
					return;
				}
			}

			Name = "HP Generic";
		}

		public override string GetPortFromMacAddressTable(csaMAC macaddress)
		{
			//Ensure Mac Address fits H.H.H. HP wont output this, but it will accept it.
            string MacAddress = macaddress.TransformHDH;

			//Mac Address is listed on first line. Port is listed on second line.
			//Only 1 MAC should be associated with a PORT, except for multicast traffic.
			//Multicast traffic will return null
			if(Connection != null && Connection.IsConnected && !String.IsNullOrEmpty(MacAddress) && !macaddress.IsMulticast)
			{
				string[] Lines = Connection.SendCommand("show mac-address " + MacAddress).Split('\n');
				if(Lines != null && Lines.Length > 0)
				{
					for(int i = 1; i < Lines.Length; i++)//Ignore first line.
					{
						string[] MacEntry = Lines[i].Split(':');
						if(MacEntry.Length == 2)
						{
							if(MacEntry[0].Trim() == "Located on Port")
							{
								return MacEntry[1].Trim();
							}
						}
					}
				}
			}

			return null;
		}

		public override csaMAC GetMacAddressFromARPTable(csaIP ipaddress)
		{
			string IPAddress = ipaddress.ToString();
			if(Connection != null && Connection.IsConnected && !String.IsNullOrEmpty(IPAddress))
			{
				string[] Lines = Connection.SendCommand("show arp").Split('\n');
				//Command doesn't include IP we don't need to skip first line
				for(int i = 0; i != Lines.Length; i++)
				{
					//Include extra space to separate 192.168.1.2 from 192.168.1.20
					if(Lines[i].Contains(IPAddress + " "))
					{
                        return new csaMAC(Lines[i].Summarize(' ').Trim().Split(' ')[1]);
					}
				}
			}

			return null;
		}

		public override string GetPortFromARPTable(AddressBase address)
		{
			if(address is csaMAC)
			{
				address = new AddressBase(csaMAC.TransformMacHDH(address.ToString()));
			}
			string Address = address.ToString();
			if(Connection != null && Connection.IsConnected && !String.IsNullOrEmpty(Address))
			{
				string[] Lines = Connection.SendCommand("show arp").Split('\n');

				//Command doesn't include IP we don't need to skip first line
				for(int i = 0; i != Lines.Length; i++)
				{
					if(Lines[i].Contains(Address))
					{
						string[] PortEntry = Lines[i].Trim().Split(' ');
						return PortEntry[PortEntry.Length - 1];
					}
				}
			}

			return null;
		}

		public override string[] GetActiveEtherChannelPorts(string EtherChannel)
		{
			if(Connection != null && Connection.IsConnected && !String.IsNullOrEmpty(EtherChannel))
			{
				List<string> Ports = new List<string>();
				//Strip to EtherChannel Number
				for(int i = 0; i != EtherChannel.Length; i++)
				{
					if(!char.IsDigit(EtherChannel, i))
					{
						EtherChannel = EtherChannel.Remove(i, 1);
						i = -1;//Reset i to 0.
					}
				}

				string[] Lines = Connection.SendCommand("show trunks").Split('\n');
				if(Lines != null && Lines.Length > 0)
				{
					for(int i = 1; i < Lines.Length; i++)
					{
						if(Lines[i].Contains("Trk" + EtherChannel))
						{
							Ports.Add(Lines[i].Trim().Split(' ')[0]);
						}
					}
				}

				if(Ports.Count > 0)
				{
					return Ports.ToArray();
				}
			}

			return null;
		}

		public override csaIP[] GetIPAddressesFromARPTable(csaMAC macaddress)
		{
			//Ensure Mac Address fits H-H
			string MacAddress = macaddress.TransformHDH;
			if(Connection != null && Connection.IsConnected && !String.IsNullOrEmpty(MacAddress))
			{
				List<csaIP> IPAddresses = new List<csaIP>();
				string[] Lines = Connection.SendCommand("show arp").Split('\n');
				//Command doesn't include IP we don't need to skip first line
				for(int i = 0; i != Lines.Length; i++)
				{
					if(Lines[i].Contains(MacAddress))
					{
                        IPAddresses.Add(new csaIP(Lines[i].Summarize(' ').Trim().Split(' ')[0]));
					}
				}

				if(IPAddresses.Count > 0)
				{
					return IPAddresses.ToArray();
				}
			}

			return null;
		}

		public override string GetPortConfiguration(string Port)
		{
			//Check NULL
			if(Connection != null && Connection.IsConnected && !String.IsNullOrEmpty(Port))
			{
				//Execute Command
				string[] Lines = Connection.SendCommand("show run").Split('\n');
				StringBuilder InterfaceLines = new StringBuilder(2000);//Stores return value.
				bool EnteredInterface = false;//Turns true once we've entered the interface configuration.

				//First line will be a repeat of what we typed in.
				for(int i = 1; i < Lines.Length; i++)
				{
					if(EnteredInterface == false && (Lines[i].Trim() == "interface " + Port.Trim()))
					{
						InterfaceLines.Append(Lines[i]);
						EnteredInterface = true;
					}
					else if (EnteredInterface == true)
					{
						if(Lines[i] == "exit")
						{
							InterfaceLines.Append(Lines[i]);
							return InterfaceLines.ToString();//Done configuring, return.
						}
						else
						{
							InterfaceLines.Append(Lines[i]);
						}
					}
				}
			}

			return null;//Something went wrong.
		}

		public override csaIP GetPortPrimaryIP(string Port)
		{
			string[] Lines = GetPortConfiguration(Port).Split('\n');
			if(Lines != null && Lines.Length > 0)
			{
				for(int i = 1; i < Lines.Length; i++)//Ignore first line.
				{
					string Line = Lines[i].TrimStart();
					if(Line.StartsWith("ip address "))
					{
						return new csaIP(Line.Split(' ')[2]);
					}
				}
			}

			return null;
		}

		public override csaIP[] GetPortIPAddresses(string Port)
		{
			string[] Lines = GetPortConfiguration(Port).Split('\n');
			List<csaIP> IPAddresses = new List<csaIP>();
			if(Lines != null && Lines.Length > 0)
			{
				for(int i = 1; i < Lines.Length; i++)//Ignore first line.
				{
					string Line = Lines[i].TrimStart();
					if(Line.StartsWith("ip address "))
					{
						IPAddresses.Add(new csaIP(Line.Split(' ')[2]));
					}
				}
			}

			if(IPAddresses.Count > 0)
			{
				return IPAddresses.ToArray();
			}
			return null;
		}

        private static ARPEntry[] GetARPEntries(string[] Entry)
        {
            List<ARPEntry> ARPEntries = new List<ARPEntry>();
            if (Entry != null && Entry.Length > 0)
            {
                for (int i = 0; i < Entry.Length; i++)
                {
                    string[] Subline = Entry[i].Summarize(' ').Trim().Split(' ');
                    if (Subline.Length == 4)
                    {
                        ARPEntries.Add(new ARPEntry(new csaIP(Subline[0]), new csaMAC(Subline[1]), Subline[3]));
                    }
                }
            }

            if (ARPEntries.Count > 0)
            {
                return ARPEntries.ToArray();
            }
            else
            {
                return null;
            }
        }
        private static MACEntry[] GetMACEntires(string[] Entry)
        {
            List<MACEntry> MACEntries = new List<MACEntry>();
            if (Entry != null && Entry.Length > 0)
            {
                for (int i = 0; i < Entry.Length; i++)
                {
                    string[] Subline = Entry[i].Summarize(' ').Trim().Split(' ');
                    if (Subline.Length == 2)
                    {
                        MACEntries.Add(new MACEntry("0", new csaMAC(Subline[0]), Subline[1]));
                    }
                }
            }
            if (MACEntries.Count > 0)
            {
                return MACEntries.ToArray();
            }
            else
            {
                return null;
            }
        }

        public override ARPEntry GetARPEntry(Surfus.TSA.csaIP ipaddress)
        {
            string IPAddress = ipaddress.ToString();
            List<string> MatchedLines = new List<string>();
            if (Connection != null && Connection.IsConnected && !String.IsNullOrEmpty(IPAddress))
            {
                string[] Lines = Connection.SendCommand("show arp").Split('\n');
                for (int i = 0; i != Lines.Length; i++)
                {
                    if(Lines[i].Contains(ipaddress.ToString()))
                    {
                        MatchedLines.Add(Lines[i]);
                    }
                }
                ARPEntry[] Entries = GetARPEntries(MatchedLines.ToArray());
                if (Entries != null)
                {
                    return Entries[0];
                }
            }
            return null;
        }
        public override ARPEntry[] GetARPEntries(Surfus.TSA.csaMAC macaddress)
        {
            string MacAddress = macaddress.TransformHDH;
            List<string> MatchedLines = new List<string>();

            if (Connection != null && Connection.IsConnected && !String.IsNullOrEmpty(MacAddress))
            {
                string[] Lines = Connection.SendCommand("show arp").Split('\n');
                for (int i = 0; i != Lines.Length; i++)
                {
                    if (Lines[i].Contains(MacAddress.ToString()))
                    {
                        MatchedLines.Add(Lines[i]);
                    }
                }
                ARPEntry[] Entries = GetARPEntries(MatchedLines.ToArray());
                if (Entries != null)
                {
                    return Entries;
                }
            }
            return null;
        }
        public override MACEntry GetMACEntry(Surfus.TSA.csaMAC macaddress)
        {
            string MacAddress = macaddress.TransformHDH;
            List<string> MatchedLines = new List<string>();

            if (Connection != null && Connection.IsConnected && !String.IsNullOrEmpty(MacAddress) && !macaddress.IsMulticast)
            {
                string[] Lines = Connection.SendCommand("show mac-address").Split('\n');
                for (int i = 0; i != Lines.Length; i++)
                {
                    if (Lines[i].Contains(MacAddress.ToString()))
                    {
                        MatchedLines.Add(Lines[i]);
                    }
                }
                MACEntry[] Entries = GetMACEntires(MatchedLines.ToArray());
                if (Entries != null)
                {
                    return Entries[0];
                }
            }
            return null;
        }

        public override CDPEntry GetCDPEntry(string Port)
        {
            List<string> IPAddresses = new List<string>();
            CDPEntry NewCDPEntry = new CDPEntry(null, null, null, null, null);
            NewCDPEntry.LocalPort = Port;
            if (Connection != null && Connection.IsConnected && Port != null)
            {
                string[] Lines = Connection.SendCommand("show cdp neighbor " + Port + " detail").Split('\n');
                for (int i = 0; i < Lines.Length; i++)
                {
                    string Line = Lines[i].Trim();
                    string LineLower = Line.ToLower();
                    if (LineLower.StartsWith("device id"))
                    {
                        NewCDPEntry.DeviceID = Line.Split(':')[1].Trim();
                    }
                    else if (LineLower.StartsWith("address"))
                    {
                        NewCDPEntry.IPAddresses = new string[] { Line.Split(':')[1].Trim() };
                    }
                    else if (LineLower.StartsWith("platform"))
                    {
                        NewCDPEntry.Platform = Line.Split(':')[1].Trim();
                    }
                    else if (LineLower.StartsWith("device port"))
                    {
                        NewCDPEntry.RemotePort = Line.Split(':')[1].Trim();
                    }
                }
            }
            return NewCDPEntry;
        }
	}
}