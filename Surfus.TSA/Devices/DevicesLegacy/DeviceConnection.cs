using System;
using System.Globalization;

namespace Surfus.TSA.Devices
{
	public abstract partial class DeviceConnection
	{
		protected Terminal _connection { get; set; }

		public virtual Terminal Connection
		{
			get
			{
				return _connection;
			}

			set
			{
				_connection = value;
			}
		}

		public string Name { get; set; }
	}
}