﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Surfus.TSA;
using Surfus.Extensions;
using Surfus.TSA.Extensions.Physical;

namespace Surfus.TSA.Devices.Cisco
{
    public class Cisco_Base : Terminal
    {
        public Cisco_Base(baseTerminal terminal) : base(terminal)
        {
            MoreSequence = "--More--";
            MoreReturn = " ";
            userPrompt = "Username:";
            passPrompt = "Password:";
        }

        public virtual bool WriteCommand(string Command)
        {
            if (SendCommand(Command).Contains("% Invalid input detected at '^' marker"))
            {
                return false;
            }
            return true;
        }
        public virtual string GetPortConfiguration(string port)
        {
            return SendCommand("show run int " + port);
        }
        public virtual ARPEntry GetARPEntry(string ipaddress)
        {
            string[] Lines = SendCommand("show arp | include Protocol|" + ipaddress + " ").Split('\n');
            return ParseARPEntries(Lines)[0];
        }
        public virtual ARPEntry[] GetARPEntries(string macaddress)
        {
            //Ensure Mac Address fits H.H.H
            string MacAddress = macaddress.TransformHHH();
            string[] Lines = SendCommand("show arp | include Protocol| " + MacAddress).Split('\n');
            return ParseARPEntries(Lines);
        }
        public virtual MACEntry GetMACEntry(string macaddress)
        {
            //Ensure Mac Address fits H.H.H
            string MacAddress = macaddress.TransformHHH();
            //Only 1 MAC should be associated with a port, except for multicast traffic.
            //Multicast traffic will return null
            if (!MacAddress.IsMulticast())
            {
                string[] Lines = SendCommand("show mac-address-table | include vlan|Vlan|VLAN|" + MacAddress).Split('\n');
                return ParseMACEntries(Lines)[0];
            }
            throw new Exception("Multicast Not Supported");
        }
        public virtual string[] GetActiveEtherChannelPorts(string EtherChannel)
        {
            List<string> Ports = new List<string>();
            //Strip to EtherChannel Number
            EtherChannel = EtherChannel.getDigits();

            string[] Lines = SendCommand("show etherchannel sum | inc Po" + EtherChannel).Split('\n');
            if (Lines != null && Lines.Length > 0)
            {
                for (int i = 0; i < Lines.Length; i++)//Ignore first line.
                {
                    string[] SplitLine = Lines[i].Split(' ');
                    for (int j = 0; j != SplitLine.Length; j++)
                    {
                        if (SplitLine[j].EndsWith("(P)"))
                        {
                            Ports.Add(SplitLine[j].Substring(0, SplitLine[j].Length - 3));
                        }
                        else if (SplitLine[j].EndsWith("(Pd)"))
                        {
                            Ports.Add(SplitLine[j].Substring(0, SplitLine[j].Length - 4));
                        }
                    }
                }
            }
            return Ports.ToArray();
        }
        public virtual string GetPortPrimaryIP(string Port)
        {
            string[] Lines = GetPortConfiguration(Port).Split('\n');
            if (Lines != null && Lines.Length > 0)
            {
                for (int i = 0; i < Lines.Length; i++)//Ignore first line.
                {
                    string Line = Lines[i].Trim();
                    if (Line.StartsWith("ip address ") && !Line.EndsWith("secondary"))
                    {
                        return Line.Split(' ')[2];
                    }
                }
            }
            throw new Exception("No IP Address");
        }
        public virtual string[] GetPortIPAddresses(string Port)
        {
            string[] Lines = GetPortConfiguration(Port).Split('\n');
            List<string> IPAddresses = new List<string>();
            if (Lines != null && Lines.Length > 0)
            {
                for (int i = 0; i < Lines.Length; i++)//Ignore first line.
                {
                    string Line = Lines[i].TrimStart();
                    if (Line.StartsWith("ip address "))
                    {
                        IPAddresses.Add(Line.Split(' ')[2]);
                    }
                }
            }
            return IPAddresses.ToArray();
        }
        public virtual CDPEntry GetCDPEntry(string Port)
        {
            List<string> IPAddresses = new List<string>();
            CDPEntry NewCDPEntry = new CDPEntry(null, null, null, null, null);
            string[] Lines = SendCommand("show cdp neighbor " + Port + " detail").Split('\n');
            for (int i = 0; i < Lines.Length; i++)
            {
                string Line = Lines[i].Trim();
                string LineLower = Line.ToLower();
                if (LineLower.StartsWith("device id: "))
                {
                    NewCDPEntry.DeviceID = Line.Substring(11);
                }
                else if (LineLower.StartsWith("ip address: "))
                {
                    IPAddresses.Add(Line.Substring(12));
                }
                else if (LineLower.StartsWith("interface: "))
                {
                    int localinterfaceIndex = 11;
                    int localinterfaceendIndex = LineLower.IndexOf(",");
                    int remoteinterfaceIndex = LineLower.LastIndexOf(" ");
                    NewCDPEntry.LocalPort = Line.Substring(localinterfaceIndex, localinterfaceendIndex - localinterfaceIndex);
                    NewCDPEntry.RemotePort = Line.Substring(remoteinterfaceIndex + 1);
                }
                else if (LineLower.StartsWith("platform: "))
                {
                    int platformIndex = 10;
                    int platformendIndex = LineLower.IndexOf(",");
                    NewCDPEntry.Platform = Line.Substring(platformIndex, platformendIndex - platformIndex);
                }
            }
            NewCDPEntry.IPAddresses = IPAddresses.ToArray();
            return NewCDPEntry;
        }

        public static MACEntry[] ParseMACEntries(string[] Entry)
        {
            List<MACEntry> MACEntries = new List<MACEntry>();
            if (Entry != null && Entry.Length > 0)
            {
                bool FoundHeader = false;
                int vlanIndex, macIndex, portIndex;
                vlanIndex = macIndex = portIndex = 0;

                for (int i = 0; i < Entry.Length; i++)
                {
                    if (FoundHeader == false)
                    {
                        string Line = Entry[i].ToLower().Replace("mac address", "mac-address");
                        Line = Line.Replace("destination address", "destination-address");
                        Line = Line.Replace("address type", "address-type");
                        Line = Line.Replace("destination port", "destination-port");
                        string[] Arguments = Line.Replace("*", "").Summarize(' ').Trim().Split(' ');
                        for (int j = 0; j < Arguments.Length; j++)
                        {
                            switch (Arguments[j])
                            {
                                case "vlan":
                                    FoundHeader = true;
                                    vlanIndex = j;
                                    break;
                                case "mac-address":
                                    FoundHeader = true;
                                    macIndex = j;
                                    break;
                                case "destination-address":
                                    macIndex = j;
                                    FoundHeader = true;
                                    break;
                                case "port":
                                    portIndex = j;
                                    FoundHeader = true;
                                    break;
                                case "ports":
                                    portIndex = j;
                                    FoundHeader = true;
                                    break;
                                case "destination-port":
                                    portIndex = j;
                                    FoundHeader = true;
                                    break;
                            }
                        }
                    }
                    else
                    {
                        string[] Arguments = Entry[i].Replace("*", "").Summarize(' ').Trim().Split(' ');
                        if (Arguments.Length >= 4)
                        {
                            MACEntries.Add(new MACEntry(Arguments[vlanIndex], Arguments[macIndex], Arguments[portIndex]));
                        }
                    }
                }
            }
            if (MACEntries.Count > 0)
            {
                return MACEntries.ToArray();
            }
            else
            {
                return null;
            }
        }
        public static ARPEntry[] ParseARPEntries(string[] Entry)
        {
            List<ARPEntry> ARPEntries = new List<ARPEntry>();
            if (Entry != null && Entry.Length > 0)
            {
                bool FoundHeader = false;
                int AddressIndex, MacIndex, PortIndex, TypeIndex;
                AddressIndex = MacIndex = PortIndex = TypeIndex = 0;
                for (int i = 1; i < Entry.Length; i++)
                {
                    if (Entry[i].StartsWith("Protocol") && FoundHeader == false)
                    {
                        FoundHeader = true;
                        AddressIndex = Entry[i].IndexOf("Address");
                        MacIndex = Entry[i].IndexOf("Hardware Addr");
                        TypeIndex = Entry[i].IndexOf("Type");
                        PortIndex = Entry[i].IndexOf("Interface");
                    }
                    else if (!Entry[i].StartsWith("Protocol") && FoundHeader)
                    {
                        if (Entry[i].Length > PortIndex)
                        {
                            int AddressLength = Entry[i].IndexOf(' ', AddressIndex) - AddressIndex;
                            int MacLength = Entry[i].IndexOf(' ', MacIndex) - MacIndex;
                            int TypeLength = Entry[i].IndexOf(' ', TypeIndex) - TypeIndex;
                            int PortLength = Entry[i].Length - PortIndex;
                            if (AddressLength > 0 && MacLength > 0 && TypeLength > 0 && PortLength > 0)
                            {
                                string Address = Entry[i].Substring(AddressIndex, AddressLength);
                                string Mac = Entry[i].Substring(MacIndex, MacLength);
                                string Port = Entry[i].Substring(PortIndex, PortLength);
                                string Type = Entry[i].Substring(TypeIndex, TypeLength);
                                ARPEntries.Add(new ARPEntry(new csaIP(Address.Trim()), new csaMAC(Mac.Trim()), Port.Trim()));
                            }
                        }
                    }
                }
            }
            if (ARPEntries.Count > 0)
            {
                return ARPEntries.ToArray();
            }
            else
            {
                return null;
            }
        }
    }
}
