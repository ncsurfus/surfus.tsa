﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Surfus.Extensions;
using Surfus.TSA.Extensions.Physical;

namespace Surfus.TSA.Devices.HP
{
    public class HP_Base : Terminal
    {
        public HP_Base(baseTerminal terminal) : base(terminal)
        {
            MoreSequence = "-- MORE --";
            MoreReturn = " ";
            userPrompt = "Username:";
            passPrompt = "Password:";
            onOpenSuccess += HP_Base_onOpenSuccess;
        }
        void HP_Base_onOpenSuccess()
        {
            if(Expect("any key", 5000) != null)
            {
                Write(LineTerminator);
            }
        }
        public virtual CDPEntry GetCDPEntry(string Port)
        {
            List<string> IPAddresses = new List<string>();
            CDPEntry NewCDPEntry = new CDPEntry(null, null, null, null, null);
            NewCDPEntry.LocalPort = Port;
            string[] Lines = SendCommand("show cdp neighbor " + Port + " detail").Split('\n');
            for (int i = 0; i < Lines.Length; i++)
            {
                string Line = Lines[i].Trim();
                string LineLower = Line.ToLower();
                if (LineLower.StartsWith("device id"))
                {
                    NewCDPEntry.DeviceID = Line.Split(':')[1].Trim();
                }
                else if (LineLower.StartsWith("address"))
                {
                    NewCDPEntry.IPAddresses = new string[] { Line.Split(':')[1].Trim() };
                }
                else if (LineLower.StartsWith("platform"))
                {
                    NewCDPEntry.Platform = Line.Split(':')[1].Trim();
                }
                else if (LineLower.StartsWith("device port"))
                {
                    NewCDPEntry.RemotePort = Line.Split(':')[1].Trim();
                }
            }
            return NewCDPEntry;
        }
        public virtual ARPEntry GetARPEntry(string ipaddress)
        {
            string IPAddress = ipaddress.ToString();
            List<string> MatchedLines = new List<string>();
            string[] Lines = SendCommand("show arp").Split('\n');
            for (int i = 0; i != Lines.Length; i++)
            {
                if (Lines[i].Contains(ipaddress))
                {
                    MatchedLines.Add(Lines[i]);
                }
            }
            ARPEntry[] Entries = ParseARPEntries(MatchedLines.ToArray());
            return Entries[0];
        }
        public virtual ARPEntry[] GetARPEntries(string macaddress)
        {
            macaddress = macaddress.TransformHDH();
            List<string> MatchedLines = new List<string>();

            string[] Lines = SendCommand("show arp").Split('\n');
            for (int i = 0; i != Lines.Length; i++)
            {
                if (Lines[i].Contains(macaddress))
                {
                    MatchedLines.Add(Lines[i]);
                }
            }
            ARPEntry[] Entries = ParseARPEntries(MatchedLines.ToArray());
            return Entries;
        }
        public virtual MACEntry GetMACEntry(string macaddress)
        {
            macaddress = macaddress.TransformHDH();
            List<string> MatchedLines = new List<string>();

            if (!macaddress.IsMulticast())
            {
                string[] Lines = SendCommand("show mac-address").Split('\n');
                for (int i = 0; i != Lines.Length; i++)
                {
                    if (Lines[i].Contains(macaddress))
                    {
                        MatchedLines.Add(Lines[i]);
                    }
                }
                MACEntry[] Entries = ParseMACEntries(MatchedLines.ToArray());
                return Entries[0];
            }
            throw new Exception("Multicast Not Supported");
        }
        public virtual string[] GetActiveEtherChannelPorts(string EtherChannel)
        {

            List<string> Ports = new List<string>();
            EtherChannel = EtherChannel.getDigits();

            string[] Lines = SendCommand("show trunks").Split('\n');
            if (Lines != null && Lines.Length > 0)
            {
                for (int i = 1; i < Lines.Length; i++)
                {
                    if (Lines[i].Contains("Trk" + EtherChannel))
                    {
                        Ports.Add(Lines[i].Trim().Split(' ')[0]);
                    }
                }
            }
            return Ports.ToArray();
        }
        public virtual string GetPortConfiguration(string Port)
        {
            //Execute Command
            string[] Lines = SendCommand("show run").Split('\n');
            StringBuilder InterfaceLines = new StringBuilder(2000);//Stores return value.
            bool EnteredInterface = false;//Turns true once we've entered the interface configuration.

            //First line will be a repeat of what we typed in.
            for (int i = 1; i < Lines.Length; i++)
            {
                if (EnteredInterface == false && (Lines[i].Trim() == "interface " + Port.Trim()))
                {
                    InterfaceLines.Append(Lines[i]);
                    EnteredInterface = true;
                }
                else if (EnteredInterface == true)
                {
                    if (Lines[i] == "exit")
                    {
                        InterfaceLines.Append(Lines[i]);
                        return InterfaceLines.ToString();//Done configuring, return.
                    }
                    else
                    {
                        InterfaceLines.Append(Lines[i]);
                    }
                }
            }
            throw new Exception("Bad Port");
        }
        public virtual string GetPortPrimaryIP(string Port)
        {
            string[] Lines = GetPortConfiguration(Port).Split('\n');
            if (Lines != null && Lines.Length > 0)
            {
                for (int i = 1; i < Lines.Length; i++)//Ignore first line.
                {
                    string Line = Lines[i].TrimStart();
                    if (Line.StartsWith("ip address "))
                    {
                        return Line.Split(' ')[2];
                    }
                }
            }

            return null;
        }
        public virtual string[] GetPortIPAddresses(string Port)
        {
            string[] Lines = GetPortConfiguration(Port).Split('\n');
            List<string> IPAddresses = new List<string>();
            if (Lines != null && Lines.Length > 0)
            {
                for (int i = 1; i < Lines.Length; i++)//Ignore first line.
                {
                    string Line = Lines[i].TrimStart();
                    if (Line.StartsWith("ip address "))
                    {
                        IPAddresses.Add(Line.Split(' ')[2]);
                    }
                }
            }
            return IPAddresses.ToArray();
        }

        private static ARPEntry[] ParseARPEntries(string[] Entry)
        {
            List<ARPEntry> ARPEntries = new List<ARPEntry>();
            if (Entry != null && Entry.Length > 0)
            {
                for (int i = 0; i < Entry.Length; i++)
                {
                    string[] Subline = Entry[i].Summarize(' ').Trim().Split(' ');
                    if (Subline.Length == 4)
                    {
                        ARPEntries.Add(new ARPEntry(new csaIP(Subline[0]), new csaMAC(Subline[1]), Subline[3]));
                    }
                }
            }

            if (ARPEntries.Count > 0)
            {
                return ARPEntries.ToArray();
            }
            else
            {
                return null;
            }
        }
        private static MACEntry[] ParseMACEntries(string[] Entry)
        {
            List<MACEntry> MACEntries = new List<MACEntry>();
            if (Entry != null && Entry.Length > 0)
            {
                for (int i = 0; i < Entry.Length; i++)
                {
                    string[] Subline = Entry[i].Summarize(' ').Trim().Split(' ');
                    if (Subline.Length == 2)
                    {
                        MACEntries.Add(new MACEntry("0", new csaMAC(Subline[0]), Subline[1]));
                    }
                }
            }
            return MACEntries.ToArray();
        }
    }
}
