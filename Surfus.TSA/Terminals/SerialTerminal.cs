﻿using System;
using System.IO;
using System.IO.Ports;
using System.Text;
using System.Linq;
using System.Security;
using System.Threading;
using System.Collections.Generic;

namespace Surfus.TSA
{
    public class SerialTerminal : baseTerminal
    {
        private SerialPort SerialInterface;

        public override bool DataAvailable
        {
            get
            {
                if (IsConnected && SerialInterface.BytesToRead > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public override bool IsConnected
        {
            get
            {
                if (SerialInterface != null)
                {
                    return SerialInterface.IsOpen;
                }
                else
                {
                    return false;
                }
            }
        }
        public override string LineTerminator { get; set; }
        public override string Hostname { get; set; }
        internal protected override bool useInternalLogin { get; protected set; }

        public SerialTerminal(string ComPort)
        {
            SerialInterface = new SerialPort(ComPort.ToUpper());
            LineTerminator = Defaults.EOL;
            Hostname = ComPort;
            useInternalLogin = false;
        }

        internal protected override bool Open()
        {
            if (!IsConnected)
            {
                if (SerialPort.GetPortNames().Contains(SerialInterface.PortName))
                {
                    try
                    {
                        SerialInterface.Open();
                    }
                    catch { }
                }
            }
            return IsConnected;
        }
        internal protected override void Close()
        {
            SerialInterface.Close();
        }

        public override string Read()
        {
                return SerialInterface.ReadExisting();
        }
        public override void Write(string Text)
        {
            SerialInterface.Write(Text);
            SerialInterface.BaseStream.Flush();
        }
    }
}
