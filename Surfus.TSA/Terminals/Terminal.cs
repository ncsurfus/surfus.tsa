﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Surfus.Extensions;
using Surfus.TSA.Extensions.Physical;
   
namespace Surfus.TSA
{
    public class Terminal
    {
        public static Terminal Create(string hostname, terminalType Type)
        {
            Terminal newTerm = null;
            if (Type == terminalType.SSH)
            {
                throw new NotSupportedException("SSH requires Username");
            }
            else if (Type == terminalType.Telnet)
            {
                newTerm = new Terminal(new TelnetTerminal(hostname));
            }
            else if (Type == terminalType.Serial)
            {
                if (System.IO.Ports.SerialPort.GetPortNames().Contains(hostname))
                {
                    newTerm = new Terminal(new SerialTerminal(hostname));
                }
                throw new Exception("Invalid comPort");
            }

            if (newTerm != null && newTerm.Open())
            {
                return newTerm;
            }
            else
            {
                throw new Exception("Bad Credentials or Hostname");
            }
        }
        public static Terminal Create(string hostname, string password, terminalType Type)
        {
            Terminal newTerm = null;
            if (Type == terminalType.SSH)
            {
                throw new NotSupportedException("SSH requires Username");
            }
            else if (Type == terminalType.Telnet)
            {
                newTerm = new Terminal(new TelnetTerminal(hostname));
            }
            else if (Type == terminalType.Serial)
            {
                if (System.IO.Ports.SerialPort.GetPortNames().Contains(hostname))
                {
                    newTerm = new Terminal(new SerialTerminal(hostname));
                }
                throw new Exception("Invalid comPort");
            }

            if (newTerm != null && newTerm.Open(password))
            {
                return newTerm;
            }
            else
            {
                throw new Exception("Bad Credentials or Hostname");
            }
        }
        public static Terminal Create(string hostname, string username, string password, terminalType Type)
        {
            Terminal newTerm = null;
            if (Type == terminalType.SSH)
            {
                newTerm = new Terminal(new sshTerminal(hostname));
            }
            else if (Type == terminalType.Telnet)
            {
                newTerm = new Terminal(new TelnetTerminal(hostname));
            }
            else if(Type == terminalType.Serial)
            {
                if (System.IO.Ports.SerialPort.GetPortNames().Contains(hostname))
                {
                    newTerm = new Terminal(new SerialTerminal(hostname));
                }
                throw new Exception("Invalid comPort");
            }
            if (newTerm.Open(username, password))
            {
                return newTerm;
            }
            throw new Exception("Bad Credentials or Hostname");
        }

        protected baseTerminal BaseTerminal;
        public event Action onOpenSuccess;

        public Terminal(baseTerminal terminal)
        {
            BaseTerminal = terminal;
            ThreadSleep = Defaults.readSleepTime;
        }

        //BaseTerminal Wrapper
        public bool IsConnected { get { return BaseTerminal.IsConnected; } }
        public bool DataAvailable { get { return BaseTerminal.DataAvailable; } }
        public string LineTerminator { get { return BaseTerminal.LineTerminator; } set { BaseTerminal.LineTerminator = value; } }
        public string Host { get { return BaseTerminal.Hostname; } set { BaseTerminal.Hostname = value; } }
        public string Read() { return BaseTerminal.Read(); }
        public void Write(string Text) { BaseTerminal.Write(Text); }
        public bool Open()
        {
            if (BaseTerminal.Open())
            {
                if(onOpenSuccess != null)
                {
                    onOpenSuccess();
                }
                return true;
            }
            return false;
        }
        public void Close() { BaseTerminal.Close(); }

        //Properties
        public int ThreadSleep { get; set; }
        public List<string> Prompts = new List<string>(Defaults.prompts);
        public string MoreSequence { get; set; }
        public string MoreReturn { get; set; }
        protected string userPrompt = ":";
        protected string passPrompt = ":";

        public virtual void WriteLine(string Text)
        {
            Write(Text + LineTerminator);
        }
        public virtual string Expect(string Parameter, int Timeout = int.MaxValue)
        {
            StringBuilder Data = new StringBuilder();
            bool ControlSequence = false;
            int totalTime = 0;
            while (!Data.Contains(Parameter) && totalTime < Timeout)
            {
                while (DataAvailable)
                {
                    ClearANSI(Read(), Data, ref ControlSequence);
                }
                totalTime += ThreadSleep;
                Thread.Sleep(ThreadSleep);
            }
            if (!Data.Contains(Parameter))
            {
                throw new Exception(Alerts.expectTimedOut);
            }
            else
            {
                return Data.ToString();
            }
        }
        public virtual string ExpectAny(string[] Parameters, int Timeout = int.MaxValue)
        {
            bool Found = false;
            StringBuilder Data = new StringBuilder();
            bool ControlSequence = false;
            int totalTime = 0;
            while (!Found && totalTime < Timeout)
            {
                while (DataAvailable)
                {
                    ClearANSI(Read(), Data, ref ControlSequence);
                }
                foreach (string Parameter in Parameters)
                {
                    if (Data.Contains(Parameter))
                    {
                        Found = true;
                    }
                }
                totalTime += ThreadSleep;
                Thread.Sleep(ThreadSleep);
            }
            if (!Found)
            {
                throw new Exception(Alerts.expectTimedOut);
            }
            {
                return Data.ToString();
            }
        }
        public virtual string ExpectAll(string[] Parameters, int Timeout = int.MaxValue)
        {
            int Found = 0;
            int Total = Parameters.Count();
            int totalTime = 0;
            StringBuilder Data = new StringBuilder();
            bool ControlSequence = false;
            while (Found != Total && totalTime < Timeout)
            {
                while (DataAvailable)
                {
                    ClearANSI(Read(), Data, ref ControlSequence);
                }
                foreach (string Parameter in Parameters)
                {
                    if (Data.Contains(Parameter))
                    {
                        Found++;
                    }
                }
                totalTime += ThreadSleep;
                Thread.Sleep(ThreadSleep);
            }
            if (Found != Total)
            {
                throw new Exception(Alerts.expectTimedOut);
            }
            else
            {
                return Data.ToString();
            }
        }
        public virtual string SendCommand(string Command)
        {
            StringBuilder Result = new StringBuilder();//Response from Terminal
            bool ControlSequence = false; //State of ANSI Control Sequence

            //Read and ignore stale data
            while (DataAvailable)
            {
                Read();
            }

            //Write command to Terminal, but don't execute
            Write(Command);

            //Read until Terminal echos command back
            while (!Result.Contains(Command))
            {
                ClearANSI(Read(), Result, ref ControlSequence);
            }

            //Execute Command
            Write(LineTerminator);

            //Reset Variables
            Result.Clear();
            ControlSequence = false;

            //Read until we end in a prompt.
            while (!Result.EndsWithAny(Prompts, true, true) && IsConnected)
            {
                while (DataAvailable)
                {
                    //Read and Cleanup ANSI. We use totalChanges because the Terminal could give us backspaces
                    int totalChanges = ClearANSI(Read(), Result, ref ControlSequence);
                    totalChanges = Result.Length - totalChanges;

                    //We hit a More statement. Press the any key! Stat!
                    if (MoreSequence != null && MoreReturn != null && Result.Contains(MoreSequence, totalChanges))
                    {
                        Write(MoreReturn);
                    }

                    //Throttle CPU usage
                    Thread.Sleep(ThreadSleep);
                }

                //Throttle CPU usage
                Thread.Sleep(ThreadSleep);
            }

            //Return Result.
            if(Result.StartsWith("\n"))
            {
                //Clear extra line from command execution
                return Result.ToString(1, Result.Length - 1);
            }
            else
            {
                return Result.ToString();
            }
        }
        public virtual string SendCommand(string Command, params object[] Arguments)
        {
            return SendCommand(String.Format(Command, Arguments));
        }
        public virtual void WriteLine(string Text, params object[] Arguments)
        {
            WriteLine(String.Format(Text, Arguments));
        }
        public virtual void Write(string Text, params object[] Arguments)
        {
            Write(String.Format(Text, Arguments));
        }

        public virtual bool Open(string Password)
        {
            return OpenWithPrompt(Password, passPrompt);
        }
        public virtual bool OpenWithPrompt(string Password, string PassPrompt)
        {
            if (BaseTerminal.useInternalLogin)
            {
                if(BaseTerminal.Open(Password))
                {
                    if (onOpenSuccess != null)
                    {
                        onOpenSuccess();
                    }
                    return true;
                }
                return false;
            }
            else
            {
                if (BaseTerminal.Open())
                {
                    if (Login(Password, PassPrompt))
                    {
                        if (onOpenSuccess != null)
                        {
                            onOpenSuccess();
                        }
                        return true;
                    }
                    return false;
                }
                return false;
            }
        }
        public virtual bool Open(string User, string Password)
        {
            return OpenWithPrompt(User, Password, userPrompt, passPrompt);
        }
        public virtual bool OpenWithPrompt(string User, string Password, string UserPrompt, string PassPrompt)
        {
            if (BaseTerminal.useInternalLogin)
            {
                if(BaseTerminal.Open(User, Password))
                {
                    if (onOpenSuccess != null)
                    {
                        onOpenSuccess();
                    }
                    return true;
                }
                return false;
            }
            else
            {
                if (BaseTerminal.Open())
                {
                    if (Login(User, UserPrompt))
                    {
                        if (Login(Password, PassPrompt))
                        {
                            if (onOpenSuccess != null)
                            {
                                onOpenSuccess();
                            }
                            return true;
                        }
                        return false;
                    }
                }
                return false;
            }
        }

        protected internal virtual bool Login(string key, string prompt)
        {
            string line = "";
            bool enteredKey = false;
            while (IsConnected)
            {
                if (DataAvailable)
                {
                    line += Read();
                    if (line.ToLower().TrimEnd().EndsWith(prompt.ToLower()))
                    {
                        WriteLine(key);
                        enteredKey = true;
                        line = "";
                    }
                    else if (line.ToLower().TrimEnd().EndsWith(prompt.ToLower()) && enteredKey)
                    {
                        return false;
                    }
                    if (enteredKey)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        public virtual string ClearANSI(string Text)
        {
            bool ControlSequence = false;//Turns true when we enter a ANSI escape sequence.
            char[] Result = new char[Text.Length];
            int RIndex = 0;
            for (int i = 0; i != Text.Length; i++)
            {
                if (Text[i] >= '\x40' && Text[i] <= '\x7E' && ControlSequence == true)//ANSI Control Sequence End
                {
                    ControlSequence = false;
                }
                else if (Text[i] == '\x1B' && i + 1 != Text.Length && Text[i + 1] == '[')//Check for ANSI Control Sequence 1
                {
                    i++;
                    ControlSequence = true;
                }
                else if (Text[i] == '\x9B')//Check for ANSI Control Sequence 2
                {
                    ControlSequence = true;
                }
                else if (Text[i] == '\x1B' && i + 1 != Text.Length && Text[i + 1] >= '\x40' && Text[i + 1] <= '\x5F' && ControlSequence == false)//Check for Escape Sequence
                {
                    i++;
                }
                else if (Text[i] == '\b' && ControlSequence == false)//Backspace is implemented. Hooray!
                {
                    //This backspaces any requested characters
                    if (RIndex > 0)
                    {
                        //Remove anything in our array.
                        RIndex--;
                    }
                }
                else if (ControlSequence == false && Text[i] != '\r')
                {
                    //Append anything else but carriage returns.
                    Result[RIndex] = Text[i];
                }
            }
            return new string(Result, 0, RIndex);
        }
        public virtual int ClearANSI(string Text, StringBuilder Buffer, ref bool ControlSequence)
        {
            int totalChanges = 0;
            //Remove ANSI escape sequences.
            for (int i = 0; i != Text.Length; i++)
            {
                if (Text[i] >= '\x40' && Text[i] <= '\x7E' && ControlSequence == true)//ANSI Control Sequence End
                {
                    ControlSequence = false;
                }
                else if (Text[i] == '\x1B' && i + 1 != Text.Length && Text[i + 1] == '[')//Check for ANSI Control Sequence 1
                {
                    i++;//We're checking two places ahead. Increment extra.
                    ControlSequence = true;
                }
                else if (Text[i] == '\x9B')//Check for ANSI Control Sequence 2
                {
                    ControlSequence = true;
                }
                else if (Text[i] == '\x1B' && i + 1 != Text.Length && Text[i + 1] >= '\x40' && Text[i + 1] <= '\x5F' && ControlSequence == false)//Check for Escape Sequence
                {
                    i++;//We're checking two places ahead. Increment extra.
                }
                else if (Text[i] == '\b' && ControlSequence == false)//Backspace is implemented. Hooray!
                {
                    //This backspaces any requested characters
                    if (Buffer.Length > 0)
                    {
                        //Remove anything in our current line.
                        Buffer.Remove(Buffer.Length - 1, 1);
                        totalChanges--;
                    }
                }
                else if (ControlSequence == false && Text[i] != '\r')
                {
                    //Process anything else but carriage returns.
                    Buffer.Append(Text[i]);
                    totalChanges++;
                }
            }
            return totalChanges;
        }
    }
}
