using System;
using System.IO;
using System.Text;
using System.Security;
using System.Threading;
using System.Collections.Generic;
using Renci.SshNet;

namespace Surfus.TSA
{
    public class sshTerminal : baseTerminal
	{
        private SshClient sshSession;
        private ShellStream SS;

        public override bool IsConnected
        {
            get
            {
                if (sshSession != null && SS != null)
                {
                    return sshSession.IsConnected;
                }
                else
                {
                    return false;
                }
            }
        }
        public override bool DataAvailable
        {
            get
            {
                if (IsConnected)
                {
                    return SS.DataAvailable;
                }
                else
                {
                    return false;
                }
            }
        }
        public override string LineTerminator { get; set; }
        public override string Hostname { get; set; }
        internal protected override bool useInternalLogin { get; protected set; }

        public sshTerminal(string hostname)
        {
            LineTerminator = "\r";
            Hostname = hostname;
            useInternalLogin = true;
        }

        internal protected override bool Open()
        {
            return false;
        }
        internal protected override bool Open(string password)
        {
            return false;
        }
        internal protected override bool Open(string username, string password)
        {
            sshSession = new SshClient(Hostname, username, password);
            sshSession.Connect();
            if (sshSession.IsConnected)
            {
                SS = sshSession.CreateShellStream("SS", 800, 240, 800, 600, 2048);
                return true;
            }
            return false;
        }

        internal protected override void Close()
        {
            SS.Close();
            sshSession.Disconnect();
        }

        public override void Write(string Text)
        {
            SS.Write(Text);
            SS.Flush();
        }
        public override string Read()
        {
            return SS.Read();
        }
	}
}