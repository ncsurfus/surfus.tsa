﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Surfus.TSA
{
    public abstract class baseTerminal
    {
        public abstract bool IsConnected { get; }
        public abstract bool DataAvailable { get; }
        public abstract string LineTerminator { get; set; }
        public abstract string Hostname { get; set; }
        internal protected abstract bool useInternalLogin { get; protected set; }

        public abstract string Read();
        public abstract void Write(string Text);

        internal protected abstract bool Open();
        internal protected virtual bool Open(string password) { throw new NotImplementedException(); }
        internal protected virtual bool Open(string username, string password) { throw new NotImplementedException(); }
        internal protected abstract void Close();
    }
}
