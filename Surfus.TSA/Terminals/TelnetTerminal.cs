using System;
using Surfus.TSA.Streams.Telnet;

namespace Surfus.TSA
{
    public class TelnetTerminal : baseTerminal
	{
        private TelnetConnection telnetSocket;

        public override bool DataAvailable
        {
            get
            {
                if (IsConnected && telnetSocket.tcpSocket.Available > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public override bool IsConnected
        {
            get
            {
                if (telnetSocket != null)
                {
                    return telnetSocket.tcpSocket.Connected;
                }
                else
                {
                    return false;
                }
            }
        }
        public override string LineTerminator { get; set; }
        public override string Hostname { get; set; }
        internal protected override bool useInternalLogin { get; protected set; }

        public TelnetTerminal(string hostname)
        {
            Hostname = hostname;
            LineTerminator = Defaults.EOL;
            useInternalLogin = false;
        }

        internal protected override bool Open()
        {
            if (!IsConnected)
            {
                telnetSocket = new TelnetConnection(Hostname, 23);
                return IsConnected;
            }
            return false;
        }
        internal protected override void Close()
        {
            telnetSocket.tcpSocket.Close();
        }

        public override string Read()
        {
            return telnetSocket.Read();
        }
        public override void Write(string Text)
        {
            telnetSocket.Write(Text);
            telnetSocket.tcpSocket.GetStream().Flush();
        }

		
	}
}