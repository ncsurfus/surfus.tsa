﻿using System;
using System.Text;
using System.IO;
using System.Collections.Generic;
using Surfus.TSA.Crypto;

namespace Surfus.TSA
{
    public class Script
    {
        public string Filepath { get; set; }
        public string Hostname { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string loginType { get; set; }
        public string deviceType { get; set; }
        public string sendType { get; set; }
        public List<string> Commands = new List<string>();
        public terminalType terminalType { get; set; }
        private Terminal Term = null;

        public Script(string filepath)
        {
            Filepath = filepath;
            sendType = "sendcommand";
            deviceType = "generic";

            Parse();
        }

        private void Parse()
        {
            if (File.Exists(Filepath))
            {
                StreamReader SR = new StreamReader(File.OpenRead(Filepath));
                if(SR.BaseStream.CanRead)
                {
                    while(!SR.EndOfStream)
                    {
                        string Line = SR.ReadLine();
                        if (Line.ToLower() == "commands:")
                        {
                            while(!SR.EndOfStream)
                            {
                                Commands.Add(SR.ReadLine());
                            }
                        }
                        else if (!Line.StartsWith("!") || !Line.StartsWith("\t"))
                        {
                            int index = Line.IndexOf(':');
                            if (index > 0)
                            {
                                string Argument = Line.Substring(0, index).ToLower().Trim();
                                string Value = Line.Substring(index + 1).ToLower().Trim();
                                switch (Argument)
                                {
                                    case "host":
                                        Hostname = Value;
                                        break;
                                    case "username":
                                        Username = Value;
                                        break;
                                    case "password":
                                        Password = Value;
                                        break;
                                    case "encryptedpassword":
                                        Password = Decrypt.PasswordFromFile(Value);
                                        break;
                                    case "logintype":
                                        loginType = Value;
                                        break;
                                    case "devicetype":
                                        deviceType = Value;
                                        break;
                                    case "sendtype":
                                        sendType = Value;
                                        break;
                                    case "terminal":
                                        terminalType = (terminalType) Enum.Parse(typeof(terminalType), Value, true);
                                        break;
                                }
                            }
                        } 
                    }
                    SR.Close();
                }
            }
            else
            {
                new FileNotFoundException(Filepath);
            }
        }
        public string Execute()
        { 
            StringBuilder Result = new StringBuilder();
            CreateTerminal();
            if(Term != null && Term.IsConnected)
            {
                foreach(string command in Commands)
                {
                    Result.AppendLine("Command: " + command);
                    if (sendType == "sendcommand")
                    {
                        Result.AppendLine(Term.SendCommand(command));
                    }
                    else if (sendType == "write")
                    {
                        Term.Write(command);
                    }
                    else if (sendType == "writeline")
                    {
                        Term.WriteLine(command);
                    }
                }
                Term.Close();
                return Result.ToString();
            }
            else
            {
                throw new Exception("Terminal Creation Failed");
            }
        }
        public void CreateTerminal()
        {
            baseTerminal baseterm = null;
            switch (terminalType)
            {
                case terminalType.SSH:
                    if(loginType != "userpass")
                    {
                        throw new Exception("Operation Not Supported");
                    }
                    baseterm = new sshTerminal(Hostname);
                    break;
                case terminalType.Telnet:
                    baseterm = new TelnetTerminal(Hostname);
                    break;
                case terminalType.Serial:
                    baseterm = new SerialTerminal(Hostname);
                    break;
            }

            switch(deviceType)
            {
                case "cisco":
                    Term = new Devices.Cisco.Cisco_Base(baseterm);
                    break;
                case "hp":
                    Term = new Devices.HP.HP_Base(baseterm);
                    break;
                case "generic":
                    Term = new Terminal(baseterm);
                    break;
            }

            switch (loginType.ToLower())
            {
                case "none":
                    Term.Open();
                    break;
                case "pass":
                    Term.Open(Password);
                    break;
                case "userpass":
                    Term.Open(Username, Password);
                    break;
            }
        }
    }
}
