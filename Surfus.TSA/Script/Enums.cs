﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Surfus.TSA.Scripting
{
    public enum loginType
    {
        userpass,
        pass,
        none
    }

    public enum deviceType
    {
        cisco,
        hp,
        generic
    }

    public enum commandType
    {
        sendcommand,
        write,
        writeline,
        expect,
        expectany,
        expectall,
        expectprompt
    }
}
