﻿using System;
using System.Text;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using Surfus.Extensions;
using Surfus.TSA.Extensions;
using Surfus.TSA.Crypto;

namespace Surfus.TSA.Scripting
{
    public class Advanced
    {
        public string Filepath { get; private set; }
        public string Hostname { get; private set; }
        public string Username { get; private set; }
        public string Password { get; private set; }
        public loginType LoginType { get; private set; }
        public deviceType DeviceType { get; private set; }
        public terminalType TerminalType { get; private set; }
        private List<Command> commands = new List<Command>();
        public Command[] Commands { get { return commands.ToArray(); } }
        private Terminal Term = null;

        public Advanced(string filepath)
        {
            Filepath = filepath;
            DeviceType = deviceType.generic;
            Parse();
        }

        private void Parse()
        {
            StreamReader stream = new StreamReader(File.OpenRead(Filepath));
            while (!stream.EndOfStream)
            {
                string Line = stream.ReadLine();
                int index = Line.IndexOf(':');
                if (index > 0 && index != Line.Length - 1)
                {
                    string Argument = Line.Substring(0, index).ToLower().Trim();
                    string Value = Line.Substring(index + 1).ToLower().Trim();
                    switch (Argument)
                    {
                        case "host":
                            Hostname = Value;
                            break;
                        case "username":
                            Username = Value;
                            break;
                        case "password":
                            Password = Value;
                            break;
                        case "encryptedpassword":
                            Password = Decrypt.PasswordFromFile(Value);
                            break;
                        case "logintype":
                            LoginType = (loginType)Enum.Parse(typeof(loginType), Value, true);
                            break;
                        case "devicetype":
                            DeviceType = (deviceType)Enum.Parse(typeof(deviceType), Value, true);
                            break;
                        case "terminal":
                            TerminalType = (terminalType)Enum.Parse(typeof(terminalType), Value, true);
                            break;
                        case "sendcommand":
                            if (Value == "begin")
                                commands.Add(new Command(commandType.sendcommand, StreamExpect(stream, "sendcommand", "end")));
                            break;
                        case "write":
                            if (Value == "begin")
                                commands.Add(new Command(commandType.write, StreamExpect(stream, "write", "end")));
                            break;
                        case "writeline":
                            if (Value == "begin")
                                commands.Add(new Command(commandType.writeline, StreamExpect(stream, "writeline", "end")));
                            break;
                        case "expect":
                            if(ProcessFormaters(Value) == "prompt")
                                commands.Add(new Command(commandType.expectprompt));
                            else
                                commands.Add(new Command(commandType.expect, ProcessFormaters(Value)));
                            break;
                        case "expectany":
                            if (!string.IsNullOrWhiteSpace(Value))
                            {
                                string[] expects = Value.Split(',');
                                expects.ForEach(x => { x.Trim(); x = ProcessFormaters(x); });
                                commands.Add(new Command(commandType.expectany, expects));
                            }
                            break;
                        case "expectall":
                            if (!string.IsNullOrWhiteSpace(Value))
                            {
                                string[] expects = Value.Split(',');
                                expects.ForEach(x => { x.Trim(); x = ProcessFormaters(x); });
                                commands.Add(new Command(commandType.expectall, expects));
                            }
                            break;
                    }
                }
            }
            stream.Close();
        }

        private string[] StreamExpect(StreamReader stream, string streamArg, string streamValue)
        {
            List<string> Result = new List<string>();
            while (!stream.EndOfStream)
            {
                string Line = stream.ReadLine();

                int index = Line.IndexOf(':');
                if (index > 0 && index != Line.Length - 1)
                {
                    string Argument = Line.Substring(0, index).ToLower().Trim();
                    string Value = Line.Substring(index + 1).ToLower().Trim();
                    if (Argument == streamArg.ToLower() && Value == streamValue.ToLower())
                    {
                        return Result.ToArray(); 
                    }
                    else
                    {
                        Result.Add(ProcessFormaters(Line));
                    }
                }
                else
                {
                    Result.Add(ProcessFormaters(Line));
                }
            }
            throw new EndOfStreamException("Command was not closed properly.");
        }

        public string Execute()
        {
            StringBuilder Result = new StringBuilder();
            CreateTerminal();
            if(Term != null && Term.IsConnected)
            {
                foreach(Command command in Commands)
                {
                    switch(command.Type)
                    {
                        case commandType.write:
                            command.Text.ForEach(x => Term.Write(x));
                            break;
                        case commandType.writeline:
                            command.Text.ForEach(x => Term.WriteLine(x) );
                            break;
                        case commandType.sendcommand:
                            command.Text.ForEach(x => Result.AppendLine(Term.SendCommand(x)) );
                            break;
                        case commandType.expect:
                            command.Text.ForEach(x => Result.AppendLine(Term.Expect(x)));
                            break;
                        case commandType.expectall:
                            Result.AppendLine(Term.ExpectAll(command.Text.ToArray()));
                            break;
                        case commandType.expectany:
                            Result.AppendLine(Term.ExpectAny(command.Text.ToArray()));
                            break;
                        case commandType.expectprompt:
                            Result.AppendLine(Term.ExpectAny(Term.Prompts.ToArray()));
                            break;
                    }
                }
                Term.Close();
                return Result.ToString();
            }
            else
            {
                throw new Exception("Terminal Creation Failed");
            }
        }
        public void CreateTerminal()
        {
            baseTerminal baseterm = null;
            switch (TerminalType)
            {
                case terminalType.SSH:
                    if(LoginType != loginType.userpass)
                    {
                        throw new Exception("Operation Not Supported");
                    }
                    baseterm = new sshTerminal(Hostname);
                    break;
                case terminalType.Telnet:
                    baseterm = new TelnetTerminal(Hostname);
                    break;
                case terminalType.Serial:
                    baseterm = new SerialTerminal(Hostname);
                    break;
            }

            switch(DeviceType)
            {
                case deviceType.cisco:
                    Term = new Devices.Cisco.Cisco_Base(baseterm);
                    break;
                case deviceType.hp:
                    Term = new Devices.HP.HP_Base(baseterm);
                    break;
                case deviceType.generic:
                    Term = new Terminal(baseterm);
                    break;
            }

            switch (LoginType)
            {
                case loginType.none:
                    Term.Open();
                    break;
                case loginType.pass:
                    Term.Open(Password);
                    break;
                case loginType.userpass:
                    Term.Open(Username, Password);
                    break;
            }
        }
        private string ProcessFormaters(string Line)
        {
            char[] NewLine = new char[Line.Length];
            int index = 0;

            for(int i = 0; i < Line.Length; i++)
            {
                if(Line[i] == '\\' && i + 1 < Line.Length)
                {
                    switch(Line[i + 1])
                    {
                        case '\\':
                            NewLine[index] = '\\'; index++;
                            break;
                        case 'r':
                            NewLine[index] = '\r'; index++;
                            break;
                        case 'n':
                            NewLine[index] = '\n'; index++;
                            break;
                        case 't':
                            NewLine[index] = '\t'; index++;
                            break;
                        case 's':
                            NewLine[index] = ' '; index++;
                            break;
                        default:
                            NewLine[index] = Line[i]; index++;
                            NewLine[index] = Line[i + 1]; index++;
                            break;
                    }
                    i++;
                }
                else
                {
                    NewLine[index] = Line[i];
                    index++;
                }
            }
            return new string(NewLine, 0, index);
        }

        public class Command
        {
            public commandType Type;
            public string[] Text;
            public Command(commandType type, string[] text)
            {
                Type = type;
                Text = text;
            }
            public Command(commandType type, string text)
            {
                Type = type;
                Text = new string[] { text };
            }
            public Command(commandType type)
            {
                Type = type;
                Text = null;
            }
        }
    }
}
