﻿using System;
using System.Management.Automation;
using Surfus.TSA;

namespace Surfus.TSA.PowerShell
{
    [Cmdlet(VerbsCommon.New, "Terminal")]
    public class new_Terminal : Cmdlet
    {
        [Parameter(Position = 0, Mandatory = true)]
        public baseTerminal Type { get; set; }

        protected override void ProcessRecord()
        {
            WriteObject(new Terminal(Type));
        }
    }

    [Cmdlet(VerbsCommon.New, "SSH")]
    public class new_SSH : Cmdlet
    {
        [Parameter(Position = 0, Mandatory = true)]
        public baseTerminal Type { get; set; }

        protected override void ProcessRecord()
        {
            WriteObject(new Terminal(Type));
        }
    }



    [Cmdlet(VerbsCommunications.Connect, "SSH")]
    public class SSH : Cmdlet
    {
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Cred1")]
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Cred2")]
        public string IPAddress { get; set; }

        [Parameter(Position = 1, Mandatory = true, ParameterSetName = "Cred1")]
        public string Username { get; set; }

        [Parameter(Position = 2, Mandatory = true, ParameterSetName = "Cred1")]
        public string Password { get; set; }

        [Parameter(Position = 1, Mandatory = false, ParameterSetName = "Cred2")]
        public PSCredential Credentials { get; set; }

        [Parameter(Position = 3, Mandatory = false, ParameterSetName = "Cred1")]
        [Parameter(Position = 2, Mandatory = false, ParameterSetName = "Cred2")]
        public string Prompt { get; set; }

        protected override void ProcessRecord()
        {
            base.ProcessRecord();

            if (IPAddress != null)
            {
                Terminal Connection = null;

                if (Username != null && Password != null)
                {
                    Connection = new Terminal(new Surfus.TSA.sshTerminal(IPAddress));
                    Connection.Open(Username, Password);
                    if (Prompt != null)
                    {
                        Connection.Prompts.Add(Prompt);
                    }
                }
                else if (Credentials != null)
                {
                    Connection = new Terminal(new Surfus.TSA.sshTerminal(IPAddress));
                    Connection.Open(Credentials.UserName, Credentials.GetNetworkCredential().Password);
                    if (Prompt != null)
                    {
                        Connection.Prompts.Add(Prompt);
                    }
                }
                WriteObject(Connection);
            }
        }
    }

    [Cmdlet(VerbsCommunications.Connect, "Telnet")]
    public class Telnet : Cmdlet
    {
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Cred1")]
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Cred2")]
        public string IPAddress { get; set; }

        [Parameter(Position = 1, Mandatory = true, ParameterSetName = "Cred1")]
        public string Username { get; set; }

        [Parameter(Position = 2, Mandatory = true, ParameterSetName = "Cred1")]
        public string Password { get; set; }

        [Parameter(Position = 1, Mandatory = false, ParameterSetName = "Cred2")]
        public PSCredential Credentials { get; set; }

        [Parameter(Position = 3, Mandatory = false, ParameterSetName = "Cred1")]
        [Parameter(Position = 2, Mandatory = false, ParameterSetName = "Cred2")]
        public string Prompt { get; set; }

        protected override void ProcessRecord()
        {
            base.ProcessRecord();
            if (IPAddress != null)
            {
                Terminal Connection;
                Connection = new Terminal(new Surfus.TSA.TelnetTerminal(IPAddress));
                if (Prompt != null)
                {
                    Connection.Prompts.Add(Prompt);
                }

                if (Username != null && Password != null)
                {
                    Connection.Open(Username, Password);
                }
                else if (Credentials != null)
                {
                    Connection.Open(Credentials.UserName, Credentials.GetNetworkCredential().Password);
                }

                WriteObject(Connection);
            }
        }
    }

    [Cmdlet(VerbsCommunications.Connect, "Serial")]
    public class Serial : Cmdlet
    {
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Cred1")]
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Cred2")]
        public string comPort { get; set; }

        [Parameter(Position = 1, Mandatory = true, ParameterSetName = "Cred1")]
        public string Username { get; set; }

        [Parameter(Position = 2, Mandatory = true, ParameterSetName = "Cred1")]
        public string Password { get; set; }

        [Parameter(Position = 1, Mandatory = false, ParameterSetName = "Cred2")]
        public PSCredential Credentials { get; set; }

        [Parameter(Position = 3, Mandatory = false, ParameterSetName = "Cred1")]
        [Parameter(Position = 2, Mandatory = false, ParameterSetName = "Cred2")]
        public string Prompt { get; set; }

        protected override void ProcessRecord()
        {
            base.ProcessRecord();
            if (comPort != null)
            {
                Terminal Connection;
                Connection = new Terminal(new Surfus.TSA.SerialTerminal(comPort));
                if (Prompt != null)
                {
                    Connection.Prompts.Add(Prompt);
                }

                if (Username != null && Password != null)
                {
                    Connection.Open(Username, Password);
                }
                else if (Credentials != null)
                {
                    Connection.Open(Credentials.UserName, Credentials.GetNetworkCredential().Password);
                }

                WriteObject(Connection);
            }
        }
    }

    [Cmdlet(VerbsCommunications.Disconnect, "Terminal")]
    public class DisconnectNetscaler : Cmdlet
    {
        [Parameter(Position = 0, Mandatory = true)]
        public PSObject Connection { get; set; }

        protected override void ProcessRecord()
        {
            base.ProcessRecord();
            if (Connection.BaseObject is Terminal)
            {
                Terminal Con = (Terminal)Connection.BaseObject;
                Con.Close();
            }
        }
    }
}
