using System;
using System.Text;
using System.Management.Automation;
using Surfus.TSA;
using Surfus.TSA.Devices;

namespace Surfus.TSA.Powershell
{
	[Cmdlet(VerbsCommunications.Connect, "Netscaler")]
	public class ConnectNetscaler : Cmdlet
	{
		[Parameter(Position = 0, Mandatory = true, ParameterSetName = "Cred1")]
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Cred2")]
		public string IPAddress { get; set; }

        [Parameter(Position = 1, Mandatory = true, ParameterSetName = "Cred1")]
		public string Username { get; set; }

        [Parameter(Position = 2, Mandatory = true, ParameterSetName = "Cred1")]
		public string Password { get; set; }

        [Parameter(Position = 1, Mandatory = false, ParameterSetName = "Cred2")]
        public PSCredential Credentials { get; set; }

        [Parameter(Position = 3, Mandatory = false, ParameterSetName = "Cred1")]
        [Parameter(Position = 2, Mandatory = false, ParameterSetName = "Cred2")]
        public bool Telnet { get; set; }

		protected override void ProcessRecord()
		{
			base.ProcessRecord();
            if (IPAddress != null && Username != null && Password != null)
            {
                Netscaler Connection = new Netscaler(!Telnet, IPAddress, Username, Password);
                WriteObject(Connection);
            }
            else if (IPAddress != null && Credentials != null)
            {
                Netscaler Connection = new Netscaler(!Telnet, IPAddress, Credentials.UserName, Credentials.GetNetworkCredential().Password);
                WriteObject(Connection);
            }
		}
	}

	[Cmdlet(VerbsCommunications.Connect, "CiscoGeneric")]
	public class ConnectCiscoGeneric : Cmdlet
	{
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Cred1")]
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Cred2")]
        public string IPAddress { get; set; }

        [Parameter(Position = 1, Mandatory = true, ParameterSetName = "Cred1")]
        public string Username { get; set; }

        [Parameter(Position = 2, Mandatory = true, ParameterSetName = "Cred1")]
        public string Password { get; set; }

        [Parameter(Position = 1, Mandatory = false, ParameterSetName = "Cred2")]
        public PSCredential Credentials { get; set; }

        [Parameter(Position = 3, Mandatory = false, ParameterSetName = "Cred1")]
        [Parameter(Position = 2, Mandatory = false, ParameterSetName = "Cred2")]
        public bool Telnet { get; set; }

		protected override void ProcessRecord()
		{
			base.ProcessRecord();
			if(IPAddress != null && Username != null && Password != null)
			{
				NetworkDevice Connection = new CiscoGeneric(!Telnet, IPAddress, Username, Password);
				WriteObject(Connection);
			}
            else if (IPAddress != null && Credentials != null)
            {
                NetworkDevice Connection = new CiscoGeneric(!Telnet, IPAddress, Credentials.UserName, Credentials.GetNetworkCredential().Password);
                WriteObject(Connection);
            }
		}
	}

	[Cmdlet(VerbsCommunications.Connect, "HPGeneric")]
	public class ConnectHPGeneric : Cmdlet
	{
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Cred1")]
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Cred2")]
        public string IPAddress { get; set; }

        [Parameter(Position = 1, Mandatory = true, ParameterSetName = "Cred1")]
        public string Username { get; set; }

        [Parameter(Position = 2, Mandatory = true, ParameterSetName = "Cred1")]
        public string Password { get; set; }

        [Parameter(Position = 1, Mandatory = false, ParameterSetName = "Cred2")]
        public PSCredential Credentials { get; set; }

        [Parameter(Position = 3, Mandatory = false, ParameterSetName = "Cred1")]
        [Parameter(Position = 2, Mandatory = false, ParameterSetName = "Cred2")]
        public bool Telnet { get; set; }

		protected override void ProcessRecord()
		{
			base.ProcessRecord();
			if(IPAddress != null && Username != null && Password != null)
			{
				NetworkDevice Connection = new HPGeneric(!Telnet, IPAddress, Username, Password);
				WriteObject(Connection);
			}
            else if (IPAddress != null && Credentials != null)
            {
                NetworkDevice Connection = new HPGeneric(!Telnet, IPAddress, Credentials.UserName, Credentials.GetNetworkCredential().Password);
                WriteObject(Connection);
            }
		}
	}

	[Cmdlet(VerbsCommunications.Disconnect, "Device")]
	public class DisconnectNetscaler : Cmdlet
	{
		[Parameter(Position = 0, Mandatory = true)]
		public PSObject Connection { get; set; }

		protected override void ProcessRecord()
		{
			base.ProcessRecord();
			if(Connection.BaseObject is Terminal)
			{
				Terminal Con = (Terminal)Connection.BaseObject;
				Con.Close();
			}
			else if(Connection.BaseObject is DeviceConnection)
			{
				DeviceConnection DevCon = (DeviceConnection)Connection.BaseObject;
				DevCon.Connection.Close();
			}
		}
	}

	[Cmdlet(VerbsCommon.Get, "CommandResult")]
	public class SendRecieveCommand : Cmdlet
	{
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "Cred1")]
		public PSObject Connection { get; set; }

		[Parameter(Position = 1, Mandatory = true)]
		public string Command { get; set; }

		protected override void ProcessRecord()
		{
			if(Connection.BaseObject is Terminal)
			{
				Terminal Con = (Terminal)Connection.BaseObject;
				WriteObject(Con.SendCommand(Command));
			}
			else if(Connection.BaseObject is DeviceConnection)
			{
				DeviceConnection DevCon = (DeviceConnection)Connection.BaseObject;
				WriteObject(DevCon.Connection.SendCommand(Command));
			}
		}
	}

	[Cmdlet(VerbsCommon.Get, "IsConnected")]
	public class IsConnected : Cmdlet
	{
		[Parameter(Position = 0, Mandatory = true)]
		public PSObject Connection { get; set; }

		protected override void ProcessRecord()
		{
			if(Connection.BaseObject is Terminal)
			{
				Terminal Con = (Terminal)Connection.BaseObject;
				WriteObject(Con.IsConnected);
			}
			else if(Connection.BaseObject is DeviceConnection)
			{
				DeviceConnection DevCon = (DeviceConnection)Connection.BaseObject;
				WriteObject(DevCon.Connection.IsConnected);
			}
		}
	}

	[Cmdlet(VerbsCommunications.Send, "Command")]
	public class SendCommand : Cmdlet
	{
		[Parameter(Position = 0, Mandatory = true)]
		public PSObject Connection { get; set; }

		[Parameter(Position = 1, Mandatory = true)]
		public string Command { get; set; }

		protected override void ProcessRecord()
		{
			if(Connection.BaseObject is Terminal)
			{
				Terminal Con = (Terminal)Connection.BaseObject;
				WriteObject(new Response(Command, Con.SendCommand(Command)));
			}
			else if(Connection.BaseObject is DeviceConnection)
			{
				DeviceConnection DevCon = (DeviceConnection)Connection.BaseObject;
				WriteObject(new Response(Command, DevCon.Connection.SendCommand(Command)));
			}
		}
	}

	[Cmdlet(VerbsCommunications.Receive, "Command")]
	public class RecieveCommand : Cmdlet
	{
        [Parameter(Position = 0, Mandatory = true, ParameterSetName = "NonPipe")]
        public Response Command { get; set; }

        [Parameter(ValueFromPipeline = true, ParameterSetName = "Pipe")]
        public Response PipelineCommand { get; set; }

		protected override void ProcessRecord()
		{
            if (Command != null)
            {
                WriteObject(Command.result);
            }
            else if (PipelineCommand != null)
            {
                WriteObject(PipelineCommand.result);
            }
		}
	}

	[Cmdlet(VerbsCommon.New, "MACAddress")]
	public class CreateMAC : Cmdlet
	{
		[Parameter(Position = 0, Mandatory = true)]
		public string Address { get; set; }

		protected override void ProcessRecord()
		{
			WriteObject(new Surfus.TSA.csaMAC(Address));
		}
	}
	
	[Cmdlet(VerbsCommon.New, "IPAddress")]
	public class CreateIP : Cmdlet
	{
		[Parameter(Position = 0, Mandatory = true)]
		public string Address { get; set; }
		
		protected override void ProcessRecord()
		{
			WriteObject(new Surfus.TSA.csaIP(Address));
		}
	}
	
	public class Response
	{
		public string command { get; set; }
		public string result { get; set; }
		public Response(string Command, string Result)
		{
			command = Command;
			result = Result;
		}
	}
	
}