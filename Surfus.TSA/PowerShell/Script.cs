﻿using System;
using System.Management.Automation;
using Surfus.TSA;

namespace Surfus.TSA.PowerShell
{
    [Cmdlet(VerbsLifecycle.Start, "Script")]
    public class StartScript : Cmdlet
    {
        [Parameter(Position = 0, Mandatory = true)]
        public string Filepath { get; set; }

        protected override void ProcessRecord()
        {
            base.ProcessRecord();
            TSA.Script script = new Script(Filepath);
            WriteObject(script.Execute());
        }
    }

    [Cmdlet(VerbsLifecycle.Start, "AdvancedScript")]
    public class StartAdvancedScript : Cmdlet
    {
        [Parameter(Position = 0, Mandatory = true)]
        public string Filepath { get; set; }

        protected override void ProcessRecord()
        {
            base.ProcessRecord();
            TSA.Scripting.Advanced script = new TSA.Scripting.Advanced(Filepath);
            WriteObject(script.Execute());
        }
    }
}
