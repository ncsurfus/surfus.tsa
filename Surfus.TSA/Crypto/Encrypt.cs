using System;
using System.Management.Automation;
using System.Management.Automation.Runspaces;

namespace Surfus.TSA.Crypto
{
	public static class Encrypt
	{
		public static void PasswordToFile(string Password, string Path)
		{
			string script = "\"" + Password +"\" | ConvertTo-SecureString -AsPlainText -Force | ConvertFrom-SecureString | Set-Content \"" + Path +"\"\r\n";
			Runspace runspace = RunspaceFactory.CreateRunspace();
			runspace.Open();
			Pipeline pipeline = runspace.CreatePipeline();
			pipeline.Commands.AddScript(script);
			var results = pipeline.Invoke();
			runspace.Close();
		}
		
		public static string Password(string Password)
		{
			string Output = "";
			string script = "\"" + Password +"\" | ConvertTo-SecureString -AsPlainText -Force | ConvertFrom-SecureString \r\n";
			Runspace runspace = RunspaceFactory.CreateRunspace();
			runspace.Open();
			Pipeline pipeline = runspace.CreatePipeline();
			pipeline.Commands.AddScript(script);
			var results = pipeline.Invoke();
			foreach (PSObject obj in results)
			{
				Output += obj.ToString();
			}
			runspace.Close();
			return Output;
		}
	}
}