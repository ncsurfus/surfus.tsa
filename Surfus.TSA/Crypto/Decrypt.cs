using System;
using System.Management.Automation;
using System.Management.Automation.Runspaces;

namespace Surfus.TSA.Crypto
{
	public static class Decrypt
	{
		public static string PasswordFromFile(string Path)
		{
			string Output = "";
            
			string script = "$EncryptedPass = Get-Content \"" + Path + "\" | ConvertTo-SecureString\r\n";
			script += "$credential = New-Object System.Management.Automation.PsCredential(\"Username\", $EncryptedPass)\r\n";
			script += "Write-Output $credential.GetNetworkCredential().Password";
			Runspace runspace = RunspaceFactory.CreateRunspace();
			runspace.Open();
			Pipeline pipeline = runspace.CreatePipeline();
			pipeline.Commands.AddScript(script);
			var results = pipeline.Invoke();
			foreach (PSObject obj in results)
			{
				Output += obj.ToString();
			}

			runspace.Close();
			return Output;
		}
		
		public static string Password(string EncryptedPassword)
		{
			string Output = "";
			string script = "$EncryptedPass = \"" + EncryptedPassword + "\" | ConvertTo-SecureString\r\n";
			script += "$credential = New-Object System.Management.Automation.PsCredential(\"Username\", $EncryptedPass)\r\n";
			script += "Write-Output $credential.GetNetworkCredential().Password";
			Runspace runspace = RunspaceFactory.CreateRunspace();
			runspace.Open();
			Pipeline pipeline = runspace.CreatePipeline();
			pipeline.Commands.AddScript(script);
			var results = pipeline.Invoke();
			foreach (PSObject obj in results)
			{
				Output += obj.ToString();
			}
			runspace.Close();
			return Output;
		}
	}
}