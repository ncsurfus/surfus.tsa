﻿using System;
using System.Text;
using System.Globalization;
using System.Net.NetworkInformation;

namespace Surfus.TSA.Extensions.Physical
{
    public static class ethernetExtensions
    {
        public static string TransformHHH(this string MacAddress)
        {
            if (!String.IsNullOrEmpty(MacAddress))
            {
                char[] FixedMac = new char[14];
                int FixedMacIndex = 0;
                for (int i = 0; i < MacAddress.Length && FixedMacIndex < FixedMac.Length; )
                {
                    if (FixedMacIndex == 4 || FixedMacIndex == 9)
                    {
                        FixedMac[FixedMacIndex] = '.';
                        FixedMacIndex++;
                    }
                    else if (char.IsLetterOrDigit(MacAddress, i))
                    {
                        FixedMac[FixedMacIndex] = MacAddress[i];
                        FixedMacIndex++;
                        i++;
                    }
                    else
                    {
                        i++;
                    }
                }

                return new string(FixedMac);
            }

            return null;
        }
        public static string TransformParse(this string MacAddress)
        {
            if (!String.IsNullOrEmpty(MacAddress))
            {
                char[] FixedMac = new char[17];
                int FixedMacIndex = 0;
                int NextParse = 2;
                for (int i = 0; i < MacAddress.Length && FixedMacIndex < FixedMac.Length; )
                {
                    if (FixedMacIndex == NextParse)
                    {
                        FixedMac[FixedMacIndex] = '-';
                        FixedMacIndex++;
                        NextParse += 3;
                    }
                    else if (char.IsLetterOrDigit(MacAddress, i))
                    {
                        FixedMac[FixedMacIndex] = MacAddress[i];
                        FixedMacIndex++;
                        i++;
                    }
                    else
                    {
                        i++;
                    }
                }

                return new string(FixedMac).ToUpper();
            }

            return null;
        }
        public static string TransformHDH(this string MacAddress)
        {
            if (!String.IsNullOrEmpty(MacAddress))
            {
                char[] FixedMac = new char[13];
                int FixedMacIndex = 0;
                for (int i = 0; i < MacAddress.Length && FixedMacIndex < FixedMac.Length; )
                {
                    if (FixedMacIndex == 6)
                    {
                        FixedMac[FixedMacIndex] = '-';
                        FixedMacIndex++;
                    }
                    else if (char.IsLetterOrDigit(MacAddress, i))
                    {
                        FixedMac[FixedMacIndex] = MacAddress[i];
                        FixedMacIndex++;
                        i++;
                    }
                    else
                    {
                        i++;
                    }
                }

                return new string(FixedMac);
            }

            return null;
        }
        public static bool IsMulticast(this string MacAddress)
        {
            if (!String.IsNullOrEmpty(MacAddress) && MacAddress.Length > 1)
            {
                string FirstOctet = MacAddress.Substring(0, 2);
                byte ParsedOctet;
                if (Byte.TryParse(FirstOctet, NumberStyles.HexNumber, null as IFormatProvider, out ParsedOctet))
                {
                    return (ParsedOctet & 1) == 1;
                }
            }

            return false;
        }
        public static string GetAddress(this PhysicalAddress Physical)
        {
            return Physical.ToString().ToLower();
        }
        public static string TransformHDH(this PhysicalAddress Physical)
        {
            return Physical.GetAddress().TransformHDH();
        }
        public static string TransformHHH(this PhysicalAddress Physical)
        {
            return Physical.GetAddress().TransformHHH();
        }
        public static bool IsMulticast(this PhysicalAddress Physical)
        {
            string MacAddress = Physical.GetAddress();
            if (!String.IsNullOrEmpty(MacAddress) && MacAddress.Length > 1)
            {
                string FirstOctet = MacAddress.Substring(0, 2);
                byte ParsedOctet;
                if (Byte.TryParse(FirstOctet, NumberStyles.HexNumber, null as IFormatProvider, out ParsedOctet))
                {
                    return (ParsedOctet & 1) == 1;
                }
            }

            return false;
        }
        public static bool Compare(this PhysicalAddress PhysicalA, PhysicalAddress PhysicalB)
        {
            string MacAddressA = PhysicalA.TransformHHH();
            string MacAddressB = PhysicalB.TransformHHH();
            if (!String.IsNullOrEmpty(MacAddressA) && !String.IsNullOrEmpty(MacAddressB))
            {
                if (MacAddressA == MacAddressB)
                {
                    return true;
                }
            }
            return false;
        }
        public static bool Compare(this PhysicalAddress PhysicalA, string PhysicalB)
        {
            string MacAddressA = PhysicalA.TransformHHH();
            string MacAddressB = PhysicalB.TransformHHH();

            if (!String.IsNullOrEmpty(MacAddressA) && !String.IsNullOrEmpty(MacAddressB))
            {
                if (MacAddressA == MacAddressB)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
