﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Surfus.Extensions
{
    public static class StringBuilderExtensions
    {
        public static bool Contains(this StringBuilder SB, string Parameter, int startIndex = 0)
        {
            int PCount = 0;
            for (int i = startIndex; i < SB.Length; i++)
            {
                if (SB[i] == Parameter[PCount])
                {
                    PCount++;
                    if (PCount == Parameter.Length)
                    {
                        return true;
                    }
                }
                else
                {
                    PCount = 0;
                }
            }
            return false;
        }
        public static int Find(this StringBuilder SB, string Parameter)
        {
            int PCount = 0;
            for (int i = 0; i != SB.Length; i++)
            {
                if (SB[i] == Parameter[PCount])
                {
                    PCount++;
                    if (PCount == Parameter.Length)
                    {
                        return i - Parameter.Length + 1;
                    }
                }
                else
                {
                    PCount = 0;
                }
            }
            return -1;
        }
        public static bool StartsWith(this StringBuilder SB, string Parameter)
        {
            if (Parameter.Length <= SB.Length)
            {
                for (int i = 0; i != Parameter.Length; i++)
                {
                    if (SB[i] != Parameter[i])
                    {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }
        public static bool EndsWith(this StringBuilder SB, string Parameter, bool IgnoreTrailingWhitespace = false, bool ignoreCase = false)
        {
            int wsOffset = 0;
            if (IgnoreTrailingWhitespace)
            {
                while (SB.Length - wsOffset - 1 >= 0 && SB[SB.Length - wsOffset - 1] == ' ')
                {
                    wsOffset++;
                }
            }

            if (Parameter.Length <= SB.Length - wsOffset)
            {
                for (int i = 0; i < Parameter.Length; i++)
                {
                    int SBIndex = SB.Length - wsOffset - Parameter.Length + i;
                    if (ignoreCase)
                    {
                        if (char.ToLower(SB[SBIndex]) != char.ToLower(Parameter[i]))
                        {
                            return false;
                        }
                    }
                    else if (SB[SBIndex] != Parameter[i])
                    {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }
        public static bool EndsWithAny(this StringBuilder SB, IEnumerable<string> values, bool IgnoreTrailingWhitespace = false, bool ignoreCase = false)
        {
            foreach (string value in values)
            {
                if (SB.EndsWith(value, IgnoreTrailingWhitespace, ignoreCase))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
