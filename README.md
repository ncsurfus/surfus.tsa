# README #

This README is a work in progress.

### What does TSA stand for? ###

* TSA stands for Terminal Scripting API.

### What does TSA attempt to accomplish? ###

* The goal of TSA is to provide an API that abstracts terminal connections and device connections.

### What is working? ###

* SSH, Telnet, and Serial
* ANSI code stripping
    * ANSI code gets stripped so only the text you would see at a terminal is returned. Backspaces are processed.
* PowerShell Integration
    * import-module works with TSA and several functions are exposed.
* Basic password crypto
    * Uses PowerShell SecureString to encrypt/decrypt password.
    * Only the encrypting user account can decrypt the password.
    * Temporary solution.
* Scripts
    * Scripts can remote into a device and run a series of commands.
    * Automate using PowerShell and Windows Task Scheduler.

### What library is used for SSH? ###

* The last stable release of SSH.NET is included and required.
    * Version: 2013.4.7
* SSH.NET can be found at https://sshnet.codeplex.com

### How do I get set up? ###

* Reference Surfus.TSA.dll in your .NET project!
* Tutorials will be up eventually.

### Who do I talk to? ###

* Repo owner, Nathan Surfus.